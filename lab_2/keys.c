#include "keys.h"

void create_keys(struct student *table, int n, struct keys key_table[COUNT])
{
    for (int i = 0; i < n; i++)
    {
        key_table[i].index = i;
        strcpy(key_table[i].key, table[i].name);
    }
}

// Сортировка таблицы ключей методом быстрой сортировки
void qsort_key (struct keys *begin, struct keys *end)
{
  struct keys *left = begin + 1, *right = end;
  struct keys x = *(left + (right - left)/2), tmp;
  while (left <= right)
  {
    while(strcmp(left->key, x.key) < 0)
      left++;
    while(strcmp(right->key, x.key) > 0)
      right--;
    if (left <= right)
    {
        tmp = *left;
        *left = *right;
        *right = tmp;
        left++;
        right--;
    }
  }
  if (begin < right)
    qsort_key(begin, right);
  if (end > left)
    qsort_key(left, end);
}

// Сортировка таблицы ключей вставками
void insertionsort_key(struct keys *begin, struct keys *end)
{
    struct keys *left = begin, *right = end;
    struct keys tmp;
    for (left = begin + 2; left < end + 1; left++)
    {
        right = left;
        while (right > begin && strcmp((right - 1)->key, right->key) > 0)
        {
            tmp = *(right-1);
            *(right-1) = *right;
            *right = tmp;
            right--;
        }
    }
}

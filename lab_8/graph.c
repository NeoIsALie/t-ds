#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct node
{
    int vertex;
    struct node* next;
};

struct Graph
{
    int numVertices;
    int* visited;
    struct node** adjLists; 
};

void DFS(struct Graph* graph, int vertex)
{
        struct node* adjList = graph->adjLists[vertex];
        struct node* temp = adjList;
        
        graph->visited[vertex] = 1;
    
        while(temp != NULL) {
            int connectedVertex = temp->vertex;
        
            if(graph->visited[connectedVertex] == 0) {
                DFS(graph, connectedVertex);
            }
            temp = temp->next;
        }       
}

 
struct node* createNode(int v)
{
    struct node* newNode = malloc(sizeof(struct node));
    newNode->vertex = v;
    newNode->next = NULL;
    return newNode;
}

struct Graph* createGraph(int vertices)
{
    struct Graph* graph = malloc(sizeof(struct Graph));
    graph->numVertices = vertices;
 
    graph->adjLists = malloc(vertices * sizeof(struct node*));
    
    graph->visited = malloc(vertices * sizeof(int));
 
    int i;
    for (i = 0; i < vertices; i++) {
        graph->adjLists[i] = NULL;
        graph->visited[i] = 0;
    }
    return graph;
}
 
void addEdge(struct Graph* graph, int src, int dest)
{
    struct node* newNode = createNode(dest);
    newNode->next = graph->adjLists[src];
    graph->adjLists[src] = newNode;
 
    newNode = createNode(src);
    newNode->next = graph->adjLists[dest];
    graph->adjLists[dest] = newNode;
}
 
void printGraph(struct Graph* graph)
{
    int v;
    for (v = 0; v < graph->numVertices; v++)
    {
        struct node* temp = graph->adjLists[v];
        printf("\n Список смежности вершины %d\n ", v + 1);
        while (temp)
        {
            printf("%d -> ", temp->vertex + 1);
            temp = temp->next;
        }
        printf("\n");
    }
}

int isConnected(struct Graph* graph)
{
    for(int i = 0; i < graph->numVertices; i++)
    {
        if(graph->visited[i] == 0)
            return 0;
    }
    return 1;
}

int main()
{
    setbuf(stdout, NULL);
    FILE* output_dot = fopen("graph.txt", "w");
    char *name = "gr";
    int vertices = 0;                                    // количество вершин в графе
    int src, dest, src1;                                 // вершины графа
    int result = 0, count = 0;
    printf("Введите число вершин в графе\n");
    if(scanf("%d", &vertices) == 1 && vertices > 0)
    {
        struct Graph* graph = createGraph(vertices);
        printf("Задайте ребра в графе в формате <вершина> <вершина>\n");
        printf("Для прекращения формирования графа введите неверный тип данных\n");
        fprintf(output_dot, "graph %s {\n", name);
        while(scanf("%d %d", &src, &dest) == 2)
        {
            if(src > 0 && dest > 0)
            {   
                if(count == 0)
                    src1 = src - 1;
                addEdge(graph, src - 1, dest - 1);
                fprintf(output_dot, "%d -- %d;\n", src, dest);
                count++;
            }
            else
            {
                printf("Неверно заданы вершины");
            }
        }
        fprintf(output_dot, "}\n");
        printf("Ввод завершен\n\n");
        printGraph(graph);
        printf("\n\n");
        DFS(graph, src1);
        result = isConnected(graph);
        if(result == 1)
            printf("Граф связный\n");
        else
            printf("Граф несвязный\n");
    }
    else
    {
        printf("Неверный ввод");
        return -1;
    }

    return 0;
}
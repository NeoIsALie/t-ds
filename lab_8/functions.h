#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__

#include "define_file.h"

void output(int **table, int size);

int input_table(FILE* f, int **table, int *size, int *kind);  // ввод графа

int into_tree(int **table, int size);  // определение, можно ли получить дерево

void export_to_dot(FILE *f, char *name,int** table, int size, int kind);  // вывод графа на экран

void clean_table(int **table, int *size);  // очистка матрицы смежности

int** allocate_matr(int n, int m); // выделение памяти на матрицу

void freeMat(int **a, int n);  // очистка памяти под матрицу

#endif


#include "define_file.h"
#include "functions.h"

int main()
{
    int rn = OK;  // код ошибки
    FILE *f_in;  // файл с данными
    int **table;  // таблица стоимостей
    int choice = -1;  // выбор пункта меню
    char *file_name = (char*)calloc(MAX_LEN,sizeof(char));  // имя файла
    int exist = 0;  // существование файла
    int size = 0;  // размер матрицы
    int tree = 0;  // можно ли превратить в дерево
    int kind = 0;  // вид графа

    table = allocate_matr(MAX_NUM, MAX_NUM);

    while (choice)
    {
        printf("** Добро пожаловать в меню **\n");
        printf("    Выберете действие:\n");
        printf("1 - ввести граф\n");
        printf("2 - вывести граф на экран\n");
        printf("3 - определить, может ли граф стать деревом\n");
        printf("0 - покинуть программу\n\n");
        printf("Ваш выбор: ");
        if (scanf("%d",&choice) != 1)
            {
                printf("Введен некорректный символ\n\n");
                rn = WRONG_SYMB;
                break;
            }
            printf("\n");
            switch (choice)
            {
                case 1: printf("    Введите название файла: ");
                        scanf("%s",file_name);
                        printf("\n");
                        f_in = fopen(file_name, "r");
                        if (f_in)
                        {
                            if (exist)
                            {
                                clean_table(table, &size);
                                exist = 0;
                                kind = 0;
                            }

                            rn = input_table(f_in, table, &size, &kind);  // считываение графа

                            if (!rn)
                            {
                                printf("    Данные введены корректно\n\n");
                                if (kind)
                                    printf("    Граф ориентированный\n\n");
                                else
                                    printf("    Граф неориентированный\n\n");
                                exist = 1;
                                //output(table, size);
                            }
                            else if (rn == NOT_INT)
                                printf("    ОШИБКА: в файле записано не натуральное число, данные не введены\n\n");
                            else if (rn == ERR_MEMORY)
                                printf("    Произошла ошибка при выделении памяти, данные не введены\n\n");
                            else
                                printf("    ОШИБКА: файл пуст, данные не введены\n\n");
                            fclose(f_in);
                        }
                        else
                        {
                            printf("    ОШИБКА: файл '%s' не найден\n\n",file_name);
                            rn = CANNOT_OPEN;
                        }

                     break;
                case 2: if (exist)
                        {
                            FILE *f_out;
                            f_out = fopen("tree.gv", "w");
                            export_to_dot(f_out, "Tree", table, size, kind);
                            fclose(f_out);
                            system("dot -Tpng tree.gv -otree.png");
                            system("shotwell tree.png");
                            printf("\n");
                        }
                        else
                            printf("    Данные еще не введены\n\n");

                    break;
                case 3: if (exist)
                        {
                            printf("    Можно ли получить дерево из графа?\n");
                            tree = 0;
                            tree = into_tree(table, size);
                            if (tree)
                            {
                                if (tree == -1)
                                    printf("    Граф уже является деревом\n\n");
                                else
                                    printf("    Ответ: Можно путем удаления вершины %d\n\n",tree);
                            }
                            else
                                printf("    Ответ: Нельзя\n\n");

                        }
                        else
                            printf("    Данные еще не введены\n\n");
                    break;
                case 0: printf("Выход из программы\n\n");
                    break;
                default: printf("Неверный пункт меню\n\n");
                    break;
        }
    }
    freeMat(table, MAX_NUM);


    return rn;
}


#ifndef __DEFINE_FILE_H__
#define __DEFINE_FILE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX_LEN 500
#define MAX_NUM 200
#define OK 0
#define WRONG_SYMB -1
#define CANNOT_OPEN -2
#define NOT_INT -3
#define ERR_MEMORY -4
#define NO_INPUT -5



#endif

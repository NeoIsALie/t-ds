#include "functions.h"

// ПАМЯТЬ

int** allocate_matr(int n, int m)  // выделение памяти на матрицу
{
    int **res;
    res = (int**)malloc(n*sizeof(int*));
    for(int i=0;i<n;i++)
    {
        res[i] = (int*)malloc(m*sizeof(int));
    }
    return res;

}

void freeMat(int **a, int n)  // очистка памяти под матрицу
{
    for(int i=0;i<n;i++)
        free(a[i]);
    free(a);
}



// ВВОД ВЫВОД

void output(int **table, int size)  // вывод таблицы стоимостей в консоль
{
    printf("%6d",1);
    for (int i = 1; i < size; i++)
        printf("%5d",i+1);
    printf("\n");
    for (int i = 0; i < size; i++)
    {
        printf("%d",i+1);
        for (int j = 0; j < size; j++)
            printf("%5d",table[i][j]);
        printf("\n");
    }
    printf("\n\n");
}

char* get_int(char *string, int *ret, int *num)  // считывание вершины
{
    *num = 0;
    *ret = OK;

    while ((*string != '-' || *string != '\n') && isdigit(*string))
    {
        *num += *string - '0';
        string++;
        *num *= 10;
    }
    *num /= 10;

    if (*string == '-' || *string == '\n')
        string++;
    else
        *ret = NOT_INT;

    return string;
}

int input_table(FILE* f, int **table, int *size, int *kind)  // ввод графа
{
    size_t n = MAX_LEN + 1;
    char *string = (char*)calloc(MAX_LEN + 1, sizeof(char));
    int count = 0;
    int a, b;
    *kind = 0;

    int ret = OK;

    while(!ret && getline(&string,&n,f) != -1)
    {
        if(*string != '\n')
        {
            count += 1;

            string = get_int(string, &ret, &a);
            if (!ret)
            {
                if (*string == '>')
                {
                    *kind = 1;
                    string++;
                }
                string = get_int(string, &ret, &b);
                if (!ret)
                {
                    if (b)
                    {
                        if (table[a-1][b-1] == 0)
                        {
                            table[a-1][b-1] = 1;
                        }
                    }
                    else
                        table[a-1][a-1] = -1;
                    if (a > *size)
                        *size = a;
                    if (b > *size)
                        *size = b;
                }
            }
        }
    }

    if (!count)
        ret = NO_INPUT;

    return ret;
}

void export_to_dot(FILE *f, char *name, int** table, int size, int kind) // вывод графа на экран
{
    char c;
    if (kind)
    {
        fprintf(f, "digraph %s {\n", name);
        c = '>';
    }
    else
    {
        fprintf(f, "graph %s {\n", name);
        c = '-';
    }

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
            if (table[i][j] && table[i][j] != -1)
                fprintf(f, "%d -%c %d;\n", i+1,c, j+1);
            else if (table[i][j] == -1)
                fprintf(f, "%d;\n", i+1);

    fprintf(f, "}\n");
}

// ЗАДАНИЕ


void get_first(int *Q, int *M, int size, int **table)  // поиск первого ребра
{
    for (int i = 0; i < size && !Q[0]; i++)
    {
        for (int j = 0; j < size && !Q[0]; j++)
        {
            if (table[i][j])
            {
                Q[0] = i + 1;
                M[i] += 1;
            }
        }
    }
}

int width(int *Q, int *M, int size, int **table)  // поиск в ширину
{
    int k = 1;
    int sides = 0;
    int ind = 0;

    while(Q[0])
    {
        ind = Q[0] - 1;
        for (int j = 0; j < size; j++)
        {
            if(table[ind][j])
            {
                Q[k] = j + 1;
                k++;
                M[j] += 1;
                if (M[j] > 1)
                    M[ind] += 1;
                sides++;
            }
        }
        for (int i = 0; i < k - 1; i++)
            Q[i] = Q[i+1];
        Q[k-1] = 0;
        for (int i = 0; i < size; i++)
            if (M[i] > size + 1)
                Q[0] = 0;
        k--;
    }
    return sides;
}

int komponent(int *M, int size, int **table, int *knots, int *on_delete, int *del)  // определение компонент связности
{
    int komp = 1;

    for (int i = 0; i < size; i++)
    {
        if (M[i])
            *knots += 1;
        else
        {
            for (int j = 0; j < size; j++)
            {
                if (table[i][j])
                {
                    on_delete[*del] = i;
                    *del += 1;
                    komp += 1;
                }

            }
        }
    }

    return komp;
}

void clean_arr(int *Q, int *M, int size)  // очистка массивов
{
    for (int i = 0; i < size; i++)
    {
        Q[i] = 0;
        M[i] = 0;
    }
}

void delete_elem(int **table, int index, int size, int** new_table)  // удаление элемента из графа
{
    for (int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
            if (i != index && j != index)
                new_table[i][j] = table[i][j];
            else
                new_table[i][j] = 0;
}


int into_tree(int **table, int size)  // определение, можно ли получить дерево
{
    int tree = -1;
    int *Q = (int*)calloc(size + 1, sizeof(int));
    int *M = (int*)calloc(size + 1, sizeof(int));
    int sides = 0, knots = 0, komp = 0, del = 0;
    int *on_delete = (int*)calloc(size + 1, sizeof(int));

    // очистка массивов
    clean_arr(Q, M, size);

    for (int i = 0; i < size; i++)
        on_delete[i] = -1;

    // поиск первой вершины
    get_first(Q,M,size, table);

    // поиск в ширину
    sides = width(Q, M, size, table);

    // подсчет узлов и проверка числа компонент связности
    komp = komponent(M, size, table, &knots, on_delete, &del);

    // поиск мешающих вершин
    if(sides > knots - 1 && tree)
    {
        for (int i = 0; i < size; i++)
        {
            if (M[i] > 1)
            {
                on_delete[del] = i;
                del++;
            }
        }
    }

    // очистка массивов
    clean_arr(Q, M, size);

    // если все еще есть шанс сделать дерево
    if (tree)
    {
        int i = 0;
        int **new_table = allocate_matr(size, size);
        while (on_delete[i] != -1 && (!tree || tree == -1))
        {
            sides = 0;
            knots = 0;

            // удаление потенциальной вершины
            delete_elem(table, on_delete[i], size, new_table);

            // поиск первой вершины
            get_first(Q,M,size, new_table);

            // поиск в ширину
            sides = width(Q,M,size, new_table);

            // подсчет узлов и проверка числа компонент связности
            int *k = (int*)calloc(size + 1, sizeof(int));
            int k_s = 0;
            komp = komponent(M, size, new_table, &knots, k, &k_s);

            // итоговая проверка вершин и ребер
            if (sides == knots - 1 && komp == 1 && !k_s)
                tree = on_delete[i] + 1;
            else
                tree = 0;
            i++;
        }
        if (tree)
        {
            for(int i = 0; i< size; i++)
                for (int j = 0; j < size; j++)
                    table[i][j] = new_table[i][j];
        }
        freeMat(new_table, size);
    }

    return tree;
}

void clean_table(int **table, int *size)  // очистка матрицы стоимостей
{
    *size = 0;
    for (int i = 0; i < MAX_NUM; i++)
        for (int j = 0; j < MAX_NUM; j++)
            table[i][j] = 0;
}

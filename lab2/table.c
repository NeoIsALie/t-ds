#include "table.h"
#include <stdlib.h>

void house_data(struct houses house)
{
    printf(" ул. %s ",house.avenue);
    printf("дом № %d ",house.number);
    printf("кв. № %d\n",house.flat);
}

void dormitory_data(struct dormitories dorm)
{
    printf("Общежитие №%d, ", dorm.number);
    printf("комната №%d\n", dorm.room);
}

void tshow(struct student human)
{
    printf("-------------------------------------------------------\n");
    printf("Имя: %s, ", human.name);
    printf("пол: ");
    switch(human.gender)
    {
        case male:
            printf("м, ");
            break;
        case female:
            printf("ж, ");
    }
    printf("\nрост: %d\n ", human.height);
    printf("Средний балл за сессию: %3.1f\n", human.midterm);
    printf("Адрес: ");
    switch(human.living)
    {
        case house:
            house_data(human.accomodation.house);
            break;
        case dorm:
            dormitory_data(human.accomodation.dormitory);
            break;
    }
    printf("-------------------------------------------------------\n");
}

// Чтение таблицы из файла
int rfile(FILE *f, struct student *human)
{
    char temp[20];
    int x;
    float y;

    if (fscanf(f, "%s", temp) && (strlen(temp) < 21) && (strlen(temp) > 0))
        strcpy(human->name, temp);
    else
        return -1;

    if (fscanf(f, "%d ", &x))
        human->gender = (enum genders) x;
    else
        return -1;

    if (fscanf(f, "%d ", &x) && (x > 0))
        human->height = x;
    else
        return -1;

    if (fscanf(f, "%f ", &y) && (y > 0))
        human->midterm = y;
    else
        return -1;

    if (fscanf(f, "%d ", &x) && (x > -1) && (x < 2))
    {
        human->living = (enum livings) x;
        if(x == 0)
        {
            char temp[30];
            if(fscanf(f, "%s", temp) && (strlen(temp) <= 30) && (strlen(temp) > 0))
                strcpy(human->accomodation.house.avenue, temp);
            else
                return -1;

            int z;
            if(fscanf(f, "%d", &z) && (z > 0))
                human->accomodation.house.number = z;
            else
                return -1;
            if(fscanf(f, "%d", &z) && (z > 0))
                human->accomodation.house.flat = z;
            else 
                return -1;
        }
        if(x == 1)
        {
            int z;
            if(fscanf(f,"%d", &z) && (z > 0))
                human->accomodation.dormitory.number = z;
            else
                return -1;
            if(fscanf(f, "%d", &z) && (z > 0))
                human->accomodation.dormitory.room = z;
            else 
                return -1;
        }
    }
    else
        return -1;

    return 0;
}

void wfile(FILE *f, struct student human)
{
    fprintf(f, "%s ", human.name);
    if(human.gender == male)
        fprintf(f, "%d ", 0);
    else if(human.gender == female)
        fprintf(f, "%d ", 1);
    fprintf(f, "%d ", human.height);
    fprintf(f, "%3.1f ", human.midterm);
    if(human.living == house)
    {
        fprintf(f, "%d ", 0);
        fprintf(f, "%s ", human.accomodation.house.avenue);
        fprintf(f, "%d ", human.accomodation.house.number);
        fprintf(f, "%d\n", human.accomodation.house.flat);
    }
    else if(human.living == dorm)
    {
        fprintf(f, "%d ", 1);
        fprintf(f, "%d ", human.accomodation.dormitory.number);
        fprintf(f, "%d\n", human.accomodation.dormitory.room);
    }
}

void add_student(FILE *f, struct student *human)
{
    char temp[30];
    int x;
    float y;
    int fl = 1;

    while(fl)
    {
        fl = 0;
        printf("Введите имя студента/студентки\n");
        if(scanf("%s", temp) && (strlen(temp) > 0) && (strlen(temp) <= 20))
            strcpy(human->name, temp);
        else
        {
            printf("Превышена допустимая длниа(20 символов)\n");
            fl = 1;
        }
    }
    fl = 1;
    while(fl)
    {
        fl = 0;
        printf("Введите пол студента/студентки\n");
        printf("0 - муж., 1 - жен.\n");
        if(scanf("%d", &x) && (x > -1) && (x < 2))
            human->gender = (enum genders) x;
        else
        {
            printf("Такого варианта нет.\n");
            fl = 1;
        }
    }
    fl = 1;
    while(fl)
    {
        fl = 0;
        printf("Введите рост студента/студентки\n");
        if(scanf("%d", &x) && (x >= 140) && (x <=200))
            human->height = x;
        else
        {
            printf("Ошибочный ввод\n");
            fl = 1;
            scanf("%*c");
        }
    }
    fl = 1;
    while(fl)
    {
        fl = 0;
        printf("Введите средний балл студента за сессию\n");
        if((scanf("%f", &y) == 1) && (y >= 0 && y < 5.1f))
            human->midterm = y;
        else
        {
            printf("Ошибочный ввод\n");
            fl = 1;
        }
    }
    fl = 1;
    while(fl)
    {
        fl = 0;
        printf("Где проживает? 0 - дом, 1 - общежитие\n");
        if(scanf("%d", &x) && (x > -1) && (x < 2))
        {
            human->living = (enum livings) x;
            if(x == 0)
            {
                printf("Введите название улицы\n");
                if(scanf("%s", temp) && (strlen(temp) > 0) && (strlen(temp) <= 30))
                    strcpy(human->accomodation.house.avenue, temp);
                else
                {
                    printf("Превышена допустимая длина(30 символов)\n");
                    fl = 1;
                }
                printf("Введите номер дома\n");
                if(scanf("%d", &x))
                    human->accomodation.house.number = x;
                else
                {
                    printf("Ошибочный ввод\n");
                    fl = 1;
                }
                printf("Введите номер квартиры\n");
                if(scanf("%d", &x))
                    human->accomodation.house.flat = x;
                else
                {
                    printf("Ошибочный ввод\n");
                    fl = 1;
                }
            }
            else if(x == 1)
            {
                printf("Введите номер общежития\n");
                if(scanf("%d", &x))
                    human->accomodation.dormitory.number = x;
               else
            {
                printf("Ошибочный ввод\n");
                fl = 1;
            }
                printf("Введите номер комнаты\n");
                if(scanf("%d", &x))
                    human->accomodation.dormitory.room = x;
                else
                {
                    printf("Ошибочный ввод\n");
                    fl = 1;
                }
            }
        }
        else
        {
            printf("Ошибочный ввод\n");
            fl = 1;
        }
    }
    wfile(f, *human);
    printf("Строка добавлена в таблицу\n");
}

void delete_student(FILE *f, struct student *table, int index, int num)
{
    for (int i = 0; i < index - 1; i++)
        wfile(f, table[i]);

    for (int i = index - 1; i < num - 1; i++)
    {
        table[i] = table[i+1];
        wfile(f, table[i]);
        
    }
    printf("Запись удалена\n");
}

void print_dorm(struct student *table, int num)
{
    int fl = 1;
    printf("Студенты, проживающие в общежитии:\n");
    for (int i = 0; i < num; i++)
        if(table[i].living == dorm)
        {
            tshow(table[i]);
            fl = 0;
        }
    if(fl)
        printf("Таких студентов нет\n");
}

// Сортировка таблицы методом быстрой сортировки
void qsort_table (struct student *begin, struct student *end)
{
  struct student *left = begin, *right = end;
  struct student x = *(left + (right - left)/2), tmp;
  while (left <= right)
  {
    while (strcmp(left->name, x.name) < 0)
      left++;
    while (strcmp(right->name, x.name) > 0)
      right--;
    if (left <= right)
    {
        tmp = *left;
        *left = *right;
        *right = tmp;
        left++;
        right--;
    }
  }
  if (begin < right)
    qsort_table (begin, right);
  if (end > left)
    qsort_table (left, end);
}

// Сортировка таблицы вставками
void insert_sort(struct student *begin, struct student *end)
{
    struct student *left = begin, *right = end;
    struct student tmp;
    for (left = begin + 1; left < end + 1; left++)
    {
        right = left;
        while (right > begin && strcmp((right - 1)->name, right->name) > 0)
        {
            tmp = *(right-1);
            *(right-1) = *right;
            *right = tmp;
            right--;
        }
    }
}

#include "table.h"
#include <stdlib.h>
#include <string.h>
void house_data(struct houses house)
{
	printf(" ул. %s ",house.avenue);
	printf("дом №%d ",house.number);
	printf("кв. №%d\n",house.flat);
}

void dormitory_data(struct dormitories dorm)
{
	printf("Общежитие №%d, ", dorm.number);
    printf("комната №%d\n", dorm.room);
}

void tshow(struct student human)
{
    printf("Имя: %s, ", human.name);
    printf("пол: ");
    switch(human.gender)
    {
    	case male:
    	    printf("м, ");
    	    break;
    	case female:
    	    printf("ж, ");
    }
    printf("\nрост: %d\n ", human.height);
    printf("Средний балл за сессию: %.1f\n", human.midterm);
    printf("Адрес: ");
    switch(human.living)
    {
    	case 0:
    	    house_data(human.accomodation.house);
            break;
    	case 1:
    	    dormitory_data(human.accomodation.dormitory);
            break;
    }
}

// Чтение таблицы из файла
int rfile(FILE *f, struct student *human)
{
    char temp[20];
    int x;
    float y;
    char *tmp;
    if (fscanf(f, "%s ", temp) && (strlen(temp) < 21) && (strlen(temp) > 0))
    {
        tmp = strtok(temp, " ");
        strcpy(human->name, tmp);
    }
    else
        return -1;

    if (fscanf(f, "%d ", &x))
    	human->gender = (enum genders) x;
    else
        return -1;

    if (fscanf(f, "%d ", &x) &&(x > 0))
        human->height = x;
    else
        return -1;

    if (fscanf(f, "%f ", &y) && (y > 0.0))
        human->midterm = y;
    else
        return -1;

    if (fscanf(f, "%d ", &x))
    {
    	if(x == 0)
    	{
            char temp[30];
    		if(fscanf(f, "%s ", temp) && (strlen(temp) <= 30) && (strlen(temp) > 0))
            {
                char *tmp;
                tmp = strtok(temp, " ");
                printf("%s\n", tmp);
    		   	strcpy(human->accomodation.house.avenue, temp);
            }
    		else
    			return -1;

    		int z;
    		if(fscanf(f, "%d ", &z) && (z > 0))
    			human->accomodation.house.number = z;
    		else
    			return -1;
    		if(fscanf(f, "%d ", &z) && (z > 0))
    			human->accomodation.house.flat = z;
    		else 
    			return -1;
    	}
    	if(x == 1)
    	{
    		int z;
    		if(fscanf(f,"%d ", &z) && (z > 0))
    			human->accomodation.dormitory.number = z;
    		else
    			return -1;
    		if(fscanf(f, "%d ", &z) && (z > 0))
    			human->accomodation.dormitory.room = z;
    		else 
    			return -1;
    	}
    }
    else
        return -1;

    return 0;
}

void wfile(FILE *f, struct student human)
{
	fprintf(f, "%s ", human.name);
    if(human.gender == male)
    	fprintf(f,"м");
    else if(human.gender == female)
    	fprintf(f, "ж");
    fprintf(f, "%d ", human.height);
    fprintf(f, "%f", human.midterm);
    switch(human.living)
    {
    	case 0:
    	    fprintf(f, "%s ", human.accomodation.house.avenue);
    	    fprintf(f, "%d ", human.accomodation.house.number);
    	    fprintf(f, "%d ", human.accomodation.house.flat);
    	    break;
    	case 1:
    	    fprintf(f, "%d ", human.accomodation.dormitory.number);
    	    fprintf(f, "%d ", human.accomodation.dormitory.room);
    	    break;
    }
}

void add_student(FILE *f, struct student *human)
{
	char temp[30];
	int x;
	float y;

	printf("Введите имя студента/студентки\n");
	if(scanf("%s", temp) && (strlen(temp) > 0) && (strlen(temp) <=20))
        strcpy(human->name, temp);
	else
	{
		printf("Ошибочный ввод\n");
        scanf("%*s");
    }
    printf("Введите пол студента/студентки\n");
    printf("0 - муж., 1 - жен.\n");
    if(scanf("%d", &x) && (x > -1) && (x < 2))
    	human->gender = (enum genders) x;
    else
    {
    	printf("Такого варианта нет.\n");
    	scanf("%*s");
    }
    printf("Введите рост студента/студентки\n");
    if(scanf("%d", &x) && (x >= 140) && (x <=200))
    	human->height = x;
    else
    {
    	printf("Ошибочный ввод\n");
        scanf("%*s");
    }
    printf("Введите средний балл студента за сессию\n");
    if(scanf("%f", &y) && (y >= 1) > (y <= 5))
    	human->midterm = y;
    else
    {
    	printf("Ошибочный ввод\n");
    	scanf("%*s");
    }
    printf("Где проживает? 0 - дом, 1 - общежитие\n");
    if(scanf("%d", &x) && (x > -1) && (x < 2))
    {
    	if(x == 0)
    	{
    		printf("Введите название улицы\n");
    		if(scanf("%s", temp) && (strlen(temp) > 0) && (strlen(temp) <= 30))
    			strcpy(human->accomodation.house.avenue, temp);
    		else
    		{
    			printf("Ошибочный ввод\n");
    			scanf("%*s");
    		}
    		printf("Введите номер дома\n");
    		if(scanf("%d", &x))
    			human->accomodation.house.number = x;
    		else
    		{
    			printf("Ошибочный ввод\n");
    			scanf("%*s");
    		}
    		printf("Введите номер квартиры\n");
    		if(scanf("%d", &x))
    			human->accomodation.house.flat = x;
    		else
    		{
    			printf("Ошибочный ввод\n");
    			scanf("%*s");
    		}
    	}
    	else if(x == 1)
    	{
    		printf("Введите номер общежития\n");
    		if(scanf("%d", &x))
    			human->accomodation.dormitory.number = x;
    		else
    		{
    			printf("Ошибочный ввод\n");
    			scanf("%*s");
    		}
    		printf("Введите номер комнаты\n");
    		if(scanf("%d", &x))
    			human->accomodation.dormitory.room = x;
    		else
    		{
    			printf("Ошибочный ввод\n");
    			scanf("%*s");
    		}
    	}
    }
    else
    {
    	printf("Ошибочный ввод\n");
    	scanf("%*s");
    }
}

void delete_student(FILE *f, struct student *table, int index, int num)
{
    for (int i = 0; i < index - 1; i++)
        wfile(f, table[i]);

    for (int i = index - 1; i < num - 1; i++)
    {
        table[i] = table[i+1];
        wfile(f, table[i]);
    }
    printf("Запись удалена\n");
}

void print_dorm(struct student *table, int num)
{
	int flag = 1;
    printf("Студенты, проживающие в общежитии:\n");
    for (int i = 0; i < num; i++)
        if ((table[i].accomodation == dormitory))
        {
            printf("%s\n", table[i].name);
            flag = 0;
        }
    if (flag)
        printf("Таких студентов нет\n");
}

// Сортировка таблицы методом быстрой сортировки
void qsort_table (struct student *begin, struct student *end)
{
  struct student *left = begin, *right = end;
  struct student x = *(left + (right - left)/2), tmp;
  while (left <= right)
  {
    while (strcmp(left->name, x.name) < 0)
      left++;
    while (strcmp(right->name, x.name) > 0)
      right--;
    if (left <= right)
    {
        tmp = *left;
        *left = *right;
        *right = tmp;
        left++;
        right--;
    }
  }
  if (begin < right)
    qsort_table (begin, right);
  if (end > left)
    qsort_table (left, end);
}

// Сортировка таблицы вставками
void insert_sort(struct student *begin, struct student *end)
{
    struct student *left = begin, *right = end;
    struct student tmp;
    for (left = begin + 1; left < end+1; left++)
    {
        right = left;
        while (right > begin && strcmp((right - 1)->name, right->name) > 0)
        {
            tmp = *(right-1);
            *(right-1) = *right;
            *right = tmp;
            right--;
        }
    }
}

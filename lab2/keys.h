#ifndef __KEYS__H__
#define __KEYS__H__

#include <stdio.h>
#include <string.h>
#include "table.h"

struct keys
{
    char key[30];
    int index;
};

void create_keys(struct student *table, int n, struct keys key_table[COUNT]);
void qsort_key (struct keys *begin, struct keys *end);
void insertionsort_key(struct keys *begin, struct keys *end);

#endif

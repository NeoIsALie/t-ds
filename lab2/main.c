#include <time.h>
#include "table.h"
#include "keys.h"

#define START 40

int main(void)
{
    setbuf(stdout, NULL);
    int pick;
    FILE *f;
    int index, num = START;
    struct student table[COUNT];
    struct keys key_table[COUNT];
    clock_t time1, time2, time3, time4, time5, time6, time7, time8;
    f = fopen("students.txt", "r");
    for (int i = 0; i < num; i++)
        rfile(f, &table[i]);
    fclose(f);

    create_keys(table, num, key_table);

    while(1)
    {
        printf("-------------------------------------------------------\n");
        printf("|         Записи с вариантами. Обработка таблиц.      |\n");
        printf("|                  Список студентов.                  |\n");
        printf("|            Варианты работы с таблицей:              |\n");
        printf("-------------------------------------------------------\n");
        printf("  0 - Показать таблицу\n");
        printf("  1 - Показать таблицу ключей\n");
        printf("  2 - Добавить строку в таблицу\n");
        printf("  3 - Удалить строку из таблицы\n");
        printf("  4 - Отсортировать таблицу ключей\n");
        printf("  5 - Отсортировать таблицу\n");
        printf("  6 - Сравнить сортировку по ключам и сортировку таблицы\n");
        printf("(быстрая сортировка и сортировка вставками)\n");
        printf("  7 - Сравнить различные сортировки (для таблицы ключей)\n");
        printf("  8 - Вывести список студентов, живущих в общежитии\n");
        printf("  9 - Выход\n");
        printf("-------------------------------------------------------\n");
        printf("Выбор: ");

        if (scanf("%d", &pick) != 1)
        {
            printf("-------------------------------------------------------\n");
            printf("Такой команды нет.\n");
            printf("-------------------------------------------------------\n");
            scanf("%*c");
            continue;
        }
        if(pick == 0)
        {
            if(num == 0)
                printf("Таблица пуста.\n");
            else
            {
                for(int i = 0; i < num; i++)
                {
                    printf("%d \n",i + 1);
                    tshow(table[i]);
                    printf("\n");
                }
            }
        }
        else if(pick == 1)
        {
            for (int i = 0; i < num; i++)
                    printf("%s       %d\n", key_table[i].key, key_table[i].index);
        }
        else if(pick == 2)
        {
            if (num >= COUNT)
                printf("Таблица заполнена\n");
            else
                {
                    f = fopen("students.txt", "a");
                    add_student(f, &table[num]);
                    fclose(f);
                    num++;
                    create_keys(table, num, key_table);
                }
        }
        else if(pick == 3)
        {
            if(num != 0)
            {
                printf("Удалить строку № ");
                if(scanf("%d", &index) == 1)
                {
                    if((index > 0) && (index < num + 1))
                    {
                        f = fopen("test.txt","w");
                        delete_student(f, table, index, num);
                        fclose(f);
                        num--;
                        create_keys(table, num, key_table);
                    }
                    else
                        printf("Неверный номер строки\n");
                }
                else
                {
                    printf("Ошибка ввода\n");
                    scanf("%*c");
                }
            }
            else
                printf("\nТаблица пуста\n");
        }
        else if(pick == 4)
        {
            insertionsort_key(key_table, key_table + num - 1);
            printf("Таблица ключей отсортирована\n");
        }
        else if(pick == 5)
        {
            insert_sort(table, table + num - 1);
            printf("Таблица отсортирована\n");
        }
        else if(pick == 6)
        {
            time1 = clock();
            qsort_key(key_table, key_table - 1);
            time2 = clock();
            time3 = clock();
            qsort_table(table, table + num*2 - 1);
            time4 = clock();

            time5 = clock();
            insertionsort_key(key_table, key_table + 3);
            time6 = clock();
            time7 = clock();
            insert_sort(table, table + num*2 -1);
            time8 = clock();

            printf("Быстрая сортировка\n");
            printf("----------------------------------------------------------\n");
            printf("| Сортировка таблицы (мс)| Сортировка таблицы ключей (мс)|\n");
            printf("----------------------------------------------------------\n");
            printf("|%24ld|%31ld|\n", time4-time3, time2-time1);
            printf("----------------------------------------------------------\n");
            printf("Сортировка вставками\n");
            printf("----------------------------------------------------------\n");
            printf("| Сортировка таблицы (мс)| Сортировка таблицы ключей (мс)|\n");
            printf("----------------------------------------------------------\n");
            printf("|%24ld|%31ld|\n", time8-time7, time6-time5);
            printf("----------------------------------------------------------\n");
        }
        else if(pick == 7)
        {
            create_keys(table, num, key_table);
            time1 = clock();
            qsort_key(key_table, key_table + num - 1);
            time2 = clock();
            create_keys(table, num, key_table);
            time3 = clock();
            insertionsort_key(key_table, key_table + num - 1); 
            time4 = clock();
            printf("------------------------------------------------\n");
            printf("|     Вставками (мс)     |     Быстрая (мс)    |\n");
            printf("------------------------------------------------\n");
            printf("|%24ld|%21ld|\n", time4-time3, time2-time1);
            printf("------------------------------------------------\n");
        }
        else if(pick == 8)
        {
            printf("-------------------------------------------------------\n");
            print_dorm(table, num);
        }
        else if(pick == 9)
        {
            printf("Выход из программы\n");
            return 0;
        }
        else
        {
            printf("Неизвестная команда\n");
            scanf("%*c");
        }
    }
    return 0;
}

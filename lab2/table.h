#ifndef TABLE_H
#define TABLE_H

#include <stdio.h>
#include <string.h>

#define COUNT 100
// пол, адрес
enum genders {male, female};
enum livings {house, dorm};
// адрес дома
struct houses{
    char avenue[30];
    int number;
    int flat;
};
// адрес общежития
struct dormitories{
    int number;
    int room;
};
// студент
struct student {
    char name[20];
    enum genders gender;
    int height;
    float midterm;
    enum livings living;
    union 
    {
        struct houses house;
        struct dormitories dormitory;
    } accomodation;
};

void tshow(struct student human);
int rfile(FILE *f, struct student *table);
void wfile(FILE *f, struct student human);
void add_student(FILE *f, struct student *human);
void delete_student(FILE *f, struct student *human, int index, int n);
void print_dorm(struct student *table, int num);
void house_data(struct houses house);
void dormitory_data(struct dormitories dorm);
void qsort_table(struct student *begin, struct student *end);
void insert_sort(struct student *begin, struct student *end);

#endif
#ifndef NUMBERS_H
#define NUMBERS_H

char fill_sign(char *num);
int fill_mantissa(char *num, int *a, int exp_pos, int dot_index, int sign_pos, int *number_length);
int fill_exponent(char *num, int exp_pos, int num_length, int *flag);
int find_dot(char *num, int num_length);
int find_exp_pos(char *num, int num_length);
char pick_sign(char a_sign_m, char b_sign_m);

#endif


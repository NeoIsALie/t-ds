#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "multiply.h"
#include "numbers.h"

int main(void)
{
    setbuf(stdout, NULL);
    printf("Умножение вещественных чисел:    \n");
    printf("---------------------------------\n");
    printf("Введите первое вещественное число\n");
    char num1[38];
    if (scanf("%s", num1) == 1)
    {
        int num1_length = strlen(num1);
        int dot1_index = find_dot(num1, num1_length);
        int exp1_index = find_exp_pos(num1, num1_length);
        if(exp1_index == -1)
        {
            printf("\nНекорректный ввод\n");
            return -1;
        }
        if(dot1_index == -1 && exp1_index == num1_length)
        {
            printf("Некорректный ввод\n");
            return -1;
        }
        printf("---------------------------------\n");
        printf("Введите второе вещественное число\n");
        char num2[38];
        if (scanf("%s", num2) == 1)
        {
            int num2_length = strlen(num2);
            int dot2_index = find_dot(num2, num2_length);
            int exp2_index = find_exp_pos(num2, num2_length);
            if(exp2_index == -1)
            {
                printf("Некорректный ввод\n");
                return -1;
            }
            if(dot2_index == -1 && exp2_index == num2_length)
            {
                printf("Некорректный ввод\n");
                return -1;
            }

            int a[30];
            int b[30];
            int result[60];

            char a_sign_m;
            char b_sign_m;
            char result_sign;
            int sign1_pos = 0;
            int sign2_pos = 0;
            int flag;
            int a_exp = 0;
            int b_exp = 0;
            int result_exp = 0;
            int result_length = 60;
            int a_length;
            int b_length;
            int stop_index;

            a_sign_m = fill_sign(num1);
            b_sign_m = fill_sign(num2);
            if(a_sign_m == '-')
                sign1_pos = 1;
            if(b_sign_m == '-')
                sign2_pos = 1;

            if(a_sign_m == '?' || b_sign_m == '?')
            {
                printf("\nНеверный знак\n");
                return -1;
            }

            zero(a, 30);
            zero(b, 30);
            zero(result, 60);

            if((fill_mantissa(num1, a, exp1_index, dot1_index, sign1_pos, &a_length) == -1) 
                || (fill_mantissa(num2, b, exp2_index, dot2_index, sign2_pos, &b_length) == -1))
            {
                 printf("Переполнение мантиссы\n");
                 return -1;
            }
            if((fill_mantissa(num1, a, exp1_index, dot1_index,sign1_pos, &a_length) == -2)
                || (fill_mantissa(num2, a, exp2_index, dot2_index,sign2_pos, &b_length) == -2))
            {
                printf("Неверный ввод мантиссы");
                return -1;
            }
            if(exp1_index != num1_length)
            {
                a_exp = fill_exponent(num1, exp1_index, num1_length, &flag);
                if(flag)
                {
                    printf("Некорректный ввод порядка");
                    return -1;
                }
            }
            else
            {
                for(int t = dot1_index + 1; t < num1_length; t++)
                {
                    if(num1[t] - '0' != 0)
                        a_exp = a_exp - (num1_length - dot1_index - 1);
                }
            }
            flag = 0;
            if(exp2_index != num2_length)
            {
                b_exp = fill_exponent(num2, exp2_index, num2_length, &flag);
                if(flag)
                {
                    printf("Некорректный ввод порядка");
                    return -1;
                }
            }
            else
            {
                for(int t = dot1_index + 1; t < num1_length; t++)
                {
                    if(num1[t] - '0' != 0)
                        a_exp = a_exp - (num2_length - dot2_index - 1);
                }
            }
            if((a_exp < -99999 || a_exp > 99999)
                || (b_exp < -99999 || b_exp > 99999))
            {
                printf("Переполнение порядка\n");
                return -1;
            }
            multiple(a, b, a_length, b_length, result);
            result_sign = pick_sign(a_sign_m, b_sign_m);
            remove_extra_zeroes(result, &result_length, &stop_index); 
            if(result_length > 31)
            {
                printf("Переполнение мантиссы после перемножения\n");
                return -1;
            }
            printf("\n");
            for(int i = result_length- 1; i >= stop_index; i--)
                printf("%d", result[i]);
            normalize(dot1_index, dot2_index, a_exp, b_exp, &result_exp);
            if(result_length == 31)
            {
                round_number(result, result_length);
            }
            if((result_exp < -99999) || (result_exp > 99999))
            {
                printf("Переполнение порядка после перемножения\n");
                return -1;
            }

            printf("---------------------------------\n");
            printf("Результат умножения\n");
            printf("---------------------------------\n");
            printf("%c", result_sign);
            printf("0.");
            for(int i = result_length + stop_index - 1; i >= stop_index; i--)
                printf("%d", result[i]);
            printf("e");
            printf("%d", result_exp);
        }
    }
    return 0;
}
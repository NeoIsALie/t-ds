#ifndef MULTIPLY_H
#define MULTIPLY_H

void zero(int *arr, int arr_length);
void multiple(int *a, int *b, int a_length, int b_length, int *result);
void multiple2(int *a, int *b, int a_length, int b_length, int *result);
void remove_extra_zeroes(int *number, int *number_length, int *stop);
void round_number(int *result, int result_length);
void normalize(int dot1_index, int dot2_index,
             int a_exp, int b_exp, int *result_exp);

#endif
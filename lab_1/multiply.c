#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "multiply.h"
// обнуление массива
void zero(int *arr, int arr_length)
{
    for(int i = 0; i < arr_length; i++)
        *(arr + i) = 0;
}
// умножение чисел
void multiple(int *a, int *b, int a_length, int b_length, int *result)
{
    int i_n1 = 0; 
    int i_n2 = 0; 
    for (int i = a_length - 1; i>= 0; i--)
    {
        int carry = 0;
        int n1 = a[i];

        i_n2 = 0;             
        for (int j = b_length - 1; j >= 0; j--)
        {
            int n2 = b[j];
            int sum = n1 * n2 + result[i_n1 + i_n2] + carry;
            carry = sum / 10;
            *(result + i_n1 + i_n2) = sum % 10;
            i_n2++;
        }
 
        if (carry > 0)
            *(result + i_n1 + i_n2) += carry;
        i_n1++;
    }
}

void multiple2(int *a, int *b, int a_length, int b_length, int *result)
{
    for (int i=0; i<a_length; i++)
    {
        int carry = 0;
        for (int j=0; j< b_length || carry; j++) 
        {
            int cur = result[i+j] + a[i] * (j < b_length ? b[j] : 0) + carry;
            result[i+j] = cur % 10;
            carry = cur / 10;
        }
    }
}
// удаление незначащих нулей 
void remove_extra_zeroes(int *number, int *number_length, int *stop)
{
    int res_length = 0;
    int zeroes = 0;
    int stop_index = 0;
    int s = 0;
    for(int i = 0; i < *number_length; i++)
    {
        if(number[i] == 0)
            zeroes++;
        if((number[i] != 0) && (s == 0))
        {
            stop_index = i;
            s = 1;
        }
    }
    res_length = *number_length - zeroes;
    *number_length = res_length;
    *stop = stop_index;
}
// округление
void round_number(int *result, int result_length)
{
    int k = 0;
    for (int i = 0; i < result_length; i++)
    {
        if (result[i] != 0)
        {                   
            k = i;
            break;
        }
    }
    if (k < 30) 
    {
        if (result[30 + k] > 5) 
        {   
            int pointer = 1;
            for (int i = 30 + k; i > k; i--)
            {
                *(result + i) = (pointer + result[i - 1]) % 10;
                pointer = (pointer + result[i - 1]) / 10;
            }
        }
    }
}
// нормализация
void normalize(int dot1_index, int dot2_index,
             int a_exp, int b_exp, int *result_exp)
{
    int res_index = dot1_index + dot2_index;
    int res_exp = a_exp + b_exp;
    printf("\n this %d ", res_exp);
    if(res_index > 1)
        res_exp = res_exp + res_index - 1;
    printf("that %d", res_exp);
    *result_exp = res_exp;
}
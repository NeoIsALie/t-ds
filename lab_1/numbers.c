#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "numbers.h"
// заполнение знака
char fill_sign(char *num)
{
    int tmp = (int)num[0];
    char sign = '?';
    if (num[0] == '-')
        sign = '-';
    else if (num[0] == '+' || num[0] == '.')
        sign = '+';
    else if(tmp > 47 && tmp < 58)
        sign = '+';
    return sign;
}
// заполнение мантиссы
int fill_mantissa(char *num, int *a, int exp_pos, int dot_index, int sign_pos, int *number_length)
{
    int i;
    int j = 0;
    int count_mantissa = 0;
    if(dot_index == -1)
    {
        for(i = sign_pos; i < exp_pos; i++)
        {
            if(count_mantissa > 30)
                return -1;
            a[j] = num[i] - '0';
            if(a[j] > 9 || a[j] < 0)
            {
                if(j == 0 && a[j] == -3)
                    j--;
                else
                    return -2;
            }
            else
            {
                count_mantissa++;
               j++;
            }
        }
    }
    else
    {   
        for (i = sign_pos; i < dot_index; i++)
        {
                if(count_mantissa > 30)
                    return -1;
                a[j] = num[i] - '0';
                if(a[j] > 9 || a[j] < 0)
                {
                    if(j == 0 && a[j] == -3)
                        j--;
                    else
                        return -2;
                }
                else
                {
                    count_mantissa++;
                   j++;
                }
        }
        for (i = dot_index + 1; i < exp_pos; i++)
        {
            if(a[0] == '-')
                j++;
            count_mantissa++;
            if(count_mantissa > 30)
                return -1;
            a[j] = num[i] - '0';
            if(a[j] > 9 || a[j] < 0)
                {
                if(j == 0 && a[j] == -3)
                    j--;
                else
                    return -2;
            }
            else
               j++;
        }
    }
    *number_length = count_mantissa;
    return 0;
}
// заполнение экспоненты
int fill_exponent(char *num, int exp_pos, int num_length, int *flag)
{
    int fl = 0;
    int exponent;
    char *token;
    char *search = "eE";

    token = strtok(num, search);
    token = strtok(NULL, search);
    
    for(int i = exp_pos + 1; i < num_length; i++)
    {
        if(num[i] - '0' >= 65 && num[i] - '0' <= 122)
        {
            fl = 1;
            break;
        }
    }
    *flag = fl;
    exponent = atoi(token);
    return exponent;
}
// поиск точки
int find_dot(char *num, int num_length)
{
    int dot_index = -1;
    int dot_count = 0;
    for (int i = 0; i < num_length; i++)
    {
        if (num[i] == '.')
        {
            dot_index = i;
            dot_count++;
        }
    }
    if (dot_count > 1)
        dot_index = -1;
    return dot_index;
}
// поиск экспоненты 
int find_exp_pos(char *num, int num_length)
{
    int exp_pos = num_length;
    for (int i = 0; i < num_length; i++)
    {
        if (num[i] == 'e' || num[i] == 'E')
            exp_pos = i;
        else if((num[i] >= 'a' && num[i] < 'e'))
            exp_pos = -1;
        else if((num[i] > 'e' && num[i] <= 'z'))
            exp_pos = -1;
        else if(num[i] > 'A' && num[i] < 'E')
            exp_pos = -1;
        else if(num[i] > 'E' && num[i] <= 'Z')
            exp_pos = -1;
    }
    return exp_pos; 
}
// определение знака
char pick_sign(char a_sign_m, char b_sign_m)
{
    char result_sign;
    if((a_sign_m == '-') && (b_sign_m == '+'))
        result_sign = '-';
    else if((a_sign_m == '+') && (b_sign_m == '-'))
        result_sign = '-';
    else
        result_sign = '+';
    return result_sign;
}

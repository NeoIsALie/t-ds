#ifndef __DEFINE_FILE_H__
#define __DEFINE_FILE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 500
#define MAX_NUM 200
#define OK 0
#define WRONG_SYMB -1
#define CANNOT_OPEN -2
#define NOT_WORD -3
#define ERR_MEMORY -4
#define NO_INPUT -5

typedef struct Tree T_t;
typedef struct BalanceTree Balance_t;
typedef struct HashClose Close_t;
typedef struct HashOpen Open_t;
typedef struct Open data_t;

struct Tree
{
    char word[MAX_LEN + 1];
    T_t *left;
    T_t *right;
};

struct BalanceTree
{
    char word[MAX_LEN + 1];
    int height;
    Balance_t *left;
    Balance_t *right;
};

struct HashClose
{
    int hash;
    int count;
    char word[MAX_LEN + 1];
};

struct Open
{
    char word[MAX_LEN + 1];
    int state;
    data_t *next;
};

struct HashOpen
{
    int hash;
    data_t *data;
};


#endif

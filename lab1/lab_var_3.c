#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void zero(int *arr, int arr_length)
{
	for(int i = 0; i < arr_length; i++)
		arr[i] = 0;
}

char fill_sign(char *num)
{
	char sign;
	if (num[0] == '-')
        sign = '-';
    else if (num[0] == '+' || ((int)num[0] > 47 && (int)num[0] < 58))
        sign = '+';
    else
   	    sign = '?';
    return sign;
}

int fill_mantissa(char *num, int *a, int exp_pos, int dot_index, int *number_length)
{
    int i;
	int j = 0;
	int count_mantissa = 0;
    for (i = 0; i < dot_index; i++)
    {
 	    count_mantissa++;
 	    if(count_mantissa > 30)
 	        return -1;
 	    a[j] = num[i] - '0';
 	    j++;
    }
    for (i = dot_index + 1; i < exp_pos; i++)
    {
        count_mantissa++;
        if(count_mantissa > 30)
            return -1;
        a[j] = num[i] - '0';
        j++;
    }
    *number_length = count_mantissa;
    return 0;
}

int fill_exponent(char *num)
{
	int exponent;
    char *token;
    char *search = "eE";

    token = strtok(num, search);
    token = strtok(NULL, search);

    exponent = atoi(token);
    return exponent;
}

int find_dot(char *num, int num_length)
{
	int dot_index;
	int dot_count = 0;
	for (int i = 0; i < num_length; i++)
	{
		if (num[i] == '.')
		{
			dot_index = i;
			dot_count++;
		}
	}
	if (dot_count > 1)
		return -1;
    return dot_index;
}

int find_exp_pos(char *num, int num_length)
{
	int exp_pos = -1;
	for (int i = 0; i < num_length; i++)
	{
		if (num[i] == 'e' || num[i] == 'E')
			exp_pos = i;
	}
    return exp_pos;
}

char pick_sign(char a_sign_m, char b_sign_m)
{
    char result_sign;
    if((a_sign_m == '-') && (b_sign_m == '+'))
        result_sign = '-';
    else if((a_sign_m == '+') && (b_sign_m == '-'))
        result_sign = '-';
    else
        result_sign = '+';
    return result_sign;
}
void multiple(int *a, int *b, int a_length, int b_length, int *result)
{
    int i_n1 = 0; 
    int i_n2 = 0; 
    for (int i = a_length - 1; i>= 0; i--)
    {
        int carry = 0;
        int n1 = a[i];

        i_n2 = 0;             
        for (int j = b_length - 1; j >= 0; j--)
        {
            int n2 = b[j];
            int sum = n1 * n2 + result[i_n1 + i_n2] + carry;
            carry = sum / 10;
            *(result + i_n1 + i_n2) = sum % 10;
            i_n2++;
        }
 
        if (carry > 0)
            *(result + i_n1 + i_n2) += carry;
        i_n1++;
    }
}
void remove_extra_zeroes(int *number, int *number_length, int *stop)
{
    int res_length = 0;
    int zeroes = 0;
    int stop_index = 0;
    int s = 0;
    for(int i = 0; i < *number_length; i++)
    {
        if(number[i] == 0)
            zeroes++;
        if((number[i] != 0) && (s == 0))
        {
            stop_index = i;
            s = 1;
        }
    }
    res_length = *number_length - zeroes;
    *number_length = res_length;
    *stop = stop_index;
}

void round_number(int *result, int result_length)
{
    int k = 0;
    for (int i = 0; i < result_length; i++)
    {
        if (result[i] != 0)
        {                   
            k = i;
            break;
        }
    }
    if (k < 30) 
    {
        if (result[30 + k] > 5) 
        {   
            int pointer = 1;
            for (int i = 30 + k; i > k; i--)
            {
                *(result + i) = (pointer + result[i - 1]) % 10;
                pointer = (pointer + result[i - 1]) / 10;
            }
        }
    }
}

void normalize(int dot1_index, int dot2_index,
             int a_exp, int b_exp, int *result_exp, int *result_index)
{
    int res_index = dot1_index + dot2_index;
    int res_exp = a_exp + b_exp;
    if(res_index > 0)
        res_exp = res_exp + res_index - 1;
    *result_index = res_index;
    *result_exp = res_exp;
}

int main(void)
{
    setbuf(stdout, NULL);
	printf("Умножение вещественных чисел:    \n");
	printf("---------------------------------\n");
	printf("Введите первое вещественное число\n");
	char num1[38];
	if (scanf("%s", &num1) == 1)
	{
		int num1_length = strlen(num1);
		int dot1_index = find_dot(num1, num1_length);
		int exp1_index = find_exp_pos(num1, num1_length);
		if((dot1_index == -1 && exp1_index == -1)
			|| (dot1_index == -1 || exp1_index == -1))
		{
			printf("\nНекорректный ввод\n");
			return -1;
		}
		printf("\n---------------------------------\n");
		printf("Введите второе вещественное число\n");
		char num2[38];
		if (scanf("%s", &num2) == 1)
		{
            int num2_length = strlen(num2);
            int dot2_index = find_dot(num2, num2_length);
            int exp2_index = find_exp_pos(num2, num2_length);
            if((dot2_index == -1 && exp2_index == -1)
			    || (dot2_index == -1 || exp2_index == -1))
		    {
			    printf("Некорректный ввод\n");
			    return -1;
		    }
		    int a[30];
            int b[30];
            int result[60];
            int *a_st;
            int *a_end;
    		int *b_st;
    		int *b_end;
    		int *res_st;
    		int *res_end;

    		char a_sign_m;
    		char b_sign_m;
            char result_sign;
            
            int a_exp;
            int b_exp;
            int result_index;
            int result_exp;
            int result_length = 60;
            int a_length;
            int b_length;
            int stop_index;

    		a_sign_m = fill_sign(num1);
    		b_sign_m = fill_sign(num2);
            if((a_sign_m == '?') || (b_sign_m == '?'))
            {
                printf("\nНеверный знак\n");
                return -1;
            }
    		a_exp = fill_exponent(num1);
    		b_exp = fill_exponent(num2);
            if((a_exp < -99999) || (a_exp > 99999))
            {
                printf("Переполнение порядка\n");
                return -1;
            }
            if((b_exp < -99999) || (b_exp > 99999))
            {
                printf("Переполнение порядка\n");
                return -1;
            }

            zero(a, 30);
            zero(b, 30);
            zero(result, 60);

            if((fill_mantissa(num1, a, exp1_index, dot1_index, &a_length) == -1) 
                || (fill_mantissa(num2, b, exp2_index, dot2_index, &b_length) == -1))
            {
                 printf("Переполнение мантиссы\n");
                 return -1;
            }
            printf("a_length = %d\n", a_length);
            printf("b_length = %d\n", b_length);
            for(int z = 0; z < a_length; z++)
                printf("%d", a[z]);
            printf("\n");
            for(int z = 0; z < b_length; z++)
                printf("%d", b[z]);
            printf("\n");
            multiple(a, b, a_length, b_length, result);
            result_sign = pick_sign(a_sign_m, b_sign_m);
            for(int z = 0; z < 60; z++)
                printf("%d",result[z]);
            remove_extra_zeroes(result, &result_length, &stop_index); 
            printf("\nresult_length = %d\n", result_length);
            printf("\nstop_index = %d\n", stop_index);
            if(result_length > 30)
            {
                printf("Переполнение мантиссы после перемножения\n");
                return -1;
            }
            round_number(result, result_length);
            normalize(dot1_index, dot2_index, a_exp, b_exp, &result_exp, &result_index);
            if((result_exp < -99999) || (result_exp > 99999))
            {
                printf("Переполнение порядка после перемножения\n");
                return -1;
            }
            printf("---------------------------------\n");
            printf("Результат умножения\n");
            printf("---------------------------------\n");
            printf("%c", result_sign);
            printf("0.");
            for(int i = result_length + stop_index - 1; i >= stop_index; i--)
                printf("%d", result[i]);
            printf("e");
            printf("%d", result_exp);
    	}
    }
    return 0;
}
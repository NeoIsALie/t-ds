#include <stdio.h>
#include <stdlib.h>
#include "spmatrix.h"
#include "nmatrix.h"
#include "timetest.h"

int main()
{
    int flag = 1;
    int pick;
    int **matrix = NULL, *vector = NULL, *result = NULL;
    int n_matr, m_matr, n_vect;
    spm_t spm_matr, spm_vect;
    spm_t *spm_res = malloc(sizeof(spm_t));

    spm_matr.A = NULL;
    spm_matr.IA = NULL;
    spm_vect.A = NULL;
    spm_vect.IA = NULL;

    while (flag)
    {
        printf("-------------------------------------------------------\n");
        printf("|             Обработка разреженных матриц            |\n");
        printf("-------------------------------------------------------\n");
        printf("  1 - Ввод матрицы\n");
        printf("  2 - Ввод вектора-строки\n");
        printf("  3 - Показать матрицу в разреженном формате\n");
        printf("  4 - Показать вектор в разреженном формате\n");
        printf("  5 - Умножение вектора-строки на матрицу в разреженном формате\n");
        printf("  6 - Умножение вектора-строки на матрицу стандартным способом\n");
        printf("  7 - Сравнение времени работы стандартного алгоритма умножения и работы с разреженными матрицами\n");
        printf("  0 - Выход\n");
        printf("Выбор:   ");

        if (scanf("%d", &pick) != 1)
            pick = -1;

        switch (pick)
        {
            case 1:
                if (matrix)
                    free_matrix(matrix, n_matr);
                matrix = input_matrix(&n_matr, &m_matr);
                if (matrix)
                {
                    spm_matr.head = NULL;
                    convert(matrix, n_matr, m_matr, &spm_matr);
                    printf("Матрица задана\n");
                }
                break;
            case 2:
                if (vector)
                    free(vector);
                vector = input_vector(&n_vect);
                if (vector)
                {
                    spm_vect.head = NULL;
                    convert(&vector, 1, n_vect, &spm_vect);
                    printf("Вектор задан\n");
                }
                break;
            case 3:
                if (spm_matr.A)
                    print_spm(spm_matr);
                else
                    printf("Матрица не задана\n");
                break;
            case 4:
                if (spm_vect.A)
                    print_spm(spm_vect);
                else
                    printf("Вектор не задан\n");
                break;
            case 5:
                if (!vector || !matrix)
                    printf("Вектор или матрица не заданы\n");
                else if (n_vect != n_matr)
                    printf("Размеры вектора и матрицы не соответствуют\n");
                else
                {
                    if (spm_res->A)
                        free_spm(spm_res);
                    spm_res->A = calloc(m_matr, sizeof(int));
                    spm_res->IA = calloc(m_matr, sizeof(int));
                    mult_spm(spm_matr, spm_vect, spm_res, n_vect);

                    if (spm_res->A && spm_res->IA)
                    {
                        printf("Результат:\n");
                        print_spm(*spm_res);
                    }
                    else
                        printf("Ошибка при выделении памяти\n");
                }
                break;
            case 6:
                if (!vector || !matrix)
                    printf("Вектор или матрица не заданы\n");
                else if (n_vect != n_matr)
                    printf("Размеры вектора и матрицы не соответствуют\n");
                else
                {
                    if (result)
                        free(result);
                    result = mult_stand(matrix, vector, n_matr, m_matr);
                    if (result)
                    {
                        printf("Результат:\n");
                        print_matrix(&result, 1, m_matr);
                    }

                    else
                        printf("Ошибка при выделении памяти\n");
                }
                break;
            case 7:
                compare();
                break;
            case 0:
                printf("Выход из программы\n");
                flag = 0;
                break;
            default:
                for (;getchar() != '\n';);
                printf("Некорректная команда\n");
                break;
        }
    }

    if (matrix)
        free_matrix(matrix, n_matr);
    if (vector)
        free(vector);
    if (result)
        free(result);
    if (spm_res)
        free_spm(spm_res);
}

#ifndef NMATRIX_H
#define NMATRIX_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_COUNT 1000

int** allocate_matrix(int n, int m);
int** input_matrix(int *n, int *m);
int* input_vector(int *n);
int rand_int(int a);
int **generate_matr(int n, int m, int percent);
int *generate_vect(int n, int percent);
void print_matrix(int **matrix, int n, int m);
void free_matrix(int **matrix, int n);
int *mult_stand(int **matrix, int *vector, int n, int r);
int** read_from_stream_matr(FILE *stream, int *n, int *m);
int input_sizes(FILE *stream, int *n, int *m);

#endif


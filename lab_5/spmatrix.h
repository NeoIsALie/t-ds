#ifndef SPMATRIX_H
#define SPMATRIX_H

#include <stdlib.h>
#include <stdio.h>

#define MAX_COUNT 1000

typedef struct spm spm_t;
typedef struct node node_t;

struct spm
{
    int *A; // массив ненулевых элементов
    int *IA; // номера строк для элементов массива A
    node_t *head; // список с номерами компонент в A и IA, соответствующих первым ненулевым элементам в столбцах
};

struct node
{
    int index;
    node_t *next;
};

int **read_spm_matr(int n, int m);
int *read_spm_vect(int n);
void print_spm(spm_t spm_matr);
void free_spm(spm_t *spm_matr);
void convert(int **matrix, int n, int m, spm_t *spm_matr);
void convert_vect(int *vector, int n, spm_t *spm_matr);
void mult_spm(spm_t spm_matr, spm_t spm_vect, spm_t *result, int n);
node_t* push(node_t *head, int x);
node_t* reverse(node_t *head);

#endif
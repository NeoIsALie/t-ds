#include "timetest.h"
#include "spmatrix.h"
#include "nmatrix.h"
#include "time.h"
// Сравнение времени работы алгоритмов
void compare()
{
    time_t t1, t2, stand_time = 0, spm_time = 0;
    int percent, k, n, m;
    int *vector, **matrix;
    spm_t sparse_matr, sparse_vect;

    printf("Введите размер вектора (от 1 до %d)\n", MAX_COUNT);
    if (scanf("%d", &k) && k > 0 && k <= MAX_COUNT)
    {
        printf("Введите размеры матрицы (от 1 до %d )\n", MAX_COUNT);
        if (scanf("%d %d", &n, &m) != 2)
        {
            for(;getchar() != '\n';);
            printf("Некорректный ввод\n");

        }
        else if (n < 1 || m < 1 || n > MAX_COUNT || m > MAX_COUNT)
            printf("Недопустимые значения\n");
        else
        {
            if (k != n)
                printf("Размеры вектора и матрицы не соответствуют\n");
            else
            {
                printf("Введите процент заполнения (от 1 до 100)\n");
                if (scanf("%d", &percent) != 1)
                {
                    for(;getchar() != '\n';);
                    printf("Некорректный ввод\n");
                }
                else if (percent < 1 || percent > 100)
                    printf("Недопустимое значения\n");
                else
                {
                    vector = generate_vect(n, percent);
                    matrix = generate_matr(n, m, percent);
                    sparse_matr.head = NULL;
                    convert(matrix, n, m, &sparse_matr);
                    sparse_vect.head = NULL;
                    convert(&vector, 1, k, &sparse_vect);
                    for(int i = 0; i < 10; i++)
                    {
                        t1 = clock();
                        mult_stand(matrix, vector, n, m);
                        t2 = clock();
                        stand_time += t2 - t1;
                    }
                    printf("Стандартное умножение:  %ld\n", stand_time/10);
                    spm_t *result = malloc(sizeof(spm_t));
                    result->A = calloc(m, sizeof(int));
                    result->IA = calloc(m, sizeof(int));
                    for(int j = 0; j < 10; j++)
                    {
                        t1 = clock();
                        mult_spm(sparse_matr, sparse_vect, result, k);
                        t2 = clock();
                        spm_time += t2 - t1;
                    }
                    printf("Умножение в разреженном формате:  %ld\n", spm_time/10);
                }
            }
        }
    }
    else
    {
        for(;getchar() != '\n';);
        printf("Некорректное значение\n");
    }
}
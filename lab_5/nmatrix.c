#include "nmatrix.h"
#include "spmatrix.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "time.h"

// Выделение памяти под матрицу
int** allocate_matrix(int n, int m)
{
    int **matrix;

    matrix = (int **)malloc(n*sizeof(int *));
    if (matrix)
    {
        for (int i = 0; i < n; i++)
        {
            matrix[i] = calloc(m, sizeof(int));
            if (!matrix[i])
                return NULL;
        }
    }

    return matrix;
}

void zero_matrix(int ** matrix, int *n, int *m)
{
    for(int i = 0; i < *n; i++)
    {
        for(int j = 0; j < *m; j++)
            matrix[i][j] = 0;
    }
}
void zero_vector(int *vector, int n)
{
    for(int i = 0; i < n; i++)
    {
        vector[i] = 0;
    }
}

int fill_non_zero(int **matrix, int *n, int *m)
{
    int choice;
    int flag = 1;
    int stop = 1;
    int x, i, j;
    printf("Введите элемент в формате <значение элемента> <индекс строки> <индекс столбца>\n");
    while (stop)
    {
            if (scanf("%d %d %d", &x,&i, &j) == 3 && (i >= 0) && (i < *n) && (j >= 0) && (j < *m))
            {
                matrix[i][j] = x;
                printf("Завершить ввод?\n0 - Нет, 1 - Да\n");

                if (scanf("%d", &choice) == 1)
                {
                    if (choice == 1)
                        stop = 0;
                    else if(choice < 0 || choice > 2)
                        printf("Некорректная команда");
                    else
                        printf("Введите элемент\n");
                }
                else
                {
                    for (;getchar() != '\n';);
                    printf("Некорректная команда\n");
                }
            }
            else
            {
                for (;getchar() != '\n';);
                printf("Некорректные входные данные\n");
                flag = 0;
            }
    }
    return flag;
}

int fill_non_zero_vect(int *vector, int *n)
{
    int choice;
    int flag = 1;
    int stop = 1;
    int x, i;
    printf("Введите элемент в формате <значение элемента><индекс>\n");
    while(stop)
    {
        if (scanf("%d %d", &x, &i) == 2 &&(i >= 0) && (i < *n))
        {
            vector[i] = x;
            printf("Завершить ввод?\n0 - Нет, 1 - Да\n");
            if(scanf("%d", &choice) == 1)
            {
                if(choice == 1)
                    stop = 0;
                else if(choice < 0 || choice > 2)
                    printf("Некорректная команда");
                else
                    printf("Введите элемент\n");
            }
            else
            {
                for (;getchar() != '\n';);
                printf("Некорректная команда\n");
            }
        }
        else
        {
            for (;getchar() != '\n';);
            printf("Некорректные входные данные\n");
            flag = 0;
        }
    }
}
// Ввод размеров матрицы
int input_sizes(FILE *stream, int *n, int *m)
{
    if (stream == stdin)
    {
        printf("Введите размеры матрицы (от 1 до %d )\n", MAX_COUNT);
        if (scanf("%d %d", n, m) != 2)
            return 0;
        else if (*n < 1 || *m < 1 || *n > MAX_COUNT || *m > MAX_COUNT)
            return 0;
        else
            return 1;
    }
    else
    {
        if (fscanf(stream, "%d %d\n", n, m) != 2)
            return 0;
        else if (*n < 1 || *m < 1 || *n > MAX_COUNT || *m > MAX_COUNT)
            return 0;
        else
            return 1;
    }
}

// Ввод матрицы
int** read_from_stream_matr(FILE *stream, int *n, int *m)
{
    int choice;
    int **matrix = NULL;
    int flag = 1, i = 0, j = 0;

    if (input_sizes(stream, n, m))
    {
        matrix = allocate_matrix(*n, *m);
        if (matrix)
        {
            zero_matrix(matrix, n, m);
            if (stream == stdin)
            {
                printf("Ввод матрицы\n");
                printf("1 - только ненулевые элементы\n");
                printf("2 - все элементы вручную\n");
                fscanf(stream, "%d", &choice);
                if(choice == 1)
                    flag = fill_non_zero(matrix, n, m);
                else
                {
                    for (i = 0; i < *n && flag; i++)
                    {
                        for (j = 0; j < *m-1 && flag; j++)
                            if (fscanf(stream, "%d ", &matrix[i][j]) != 1)
                                flag = 0;
                        if (fscanf(stream, "%d", &matrix[i][j]) != 1)
                            flag = 0;
                    }
                }
            }
            else
            {
                printf("Ввод матрицы\n");
                for (i = 0; i < *n && flag; i++)
                {
                    for (j = 0; j < *m-1 && flag; j++)
                        if (fscanf(stream, "%d ", &matrix[i][j]) != 1)
                            flag = 0;
                    if (fscanf(stream, "%d", &matrix[i][j]) != 1)
                        flag = 0;
                }
            }
            if (!flag)
            {
                if (stream == stdin)
                    for(;getchar() != '\n';);
                printf("Некорректные входные данные\n");
                matrix = NULL;
            }
        }
        else
            printf("Ошибка при выделении памяти\n");
    }
    else
    {
        if (stream == stdin)
            for(;getchar() != '\n';);
        printf("Некоректные входные данные\n");
    }

    return matrix;
}

// Ввод вектора
int* read_from_stream_vect(FILE *stream, int *n)
{
    int choice;
    int *vector = NULL;
    int flag = 1, i = 0;

    if (stream == stdin)
        printf("Введите размер вектора (от 1 до %d)\n", MAX_COUNT);

    if (fscanf(stream, "%d", n) == 1 && (*n > 0) && (*n <= MAX_COUNT))
    {
        vector = calloc(*n, sizeof(int));
        if (vector)
        {
            if (stream == stdin)
            {
                printf("Ввод вектора\n");
                printf("1 - только ненулевые элементы\n");
                printf("2 - полностью вручную\n");
                fscanf(stream, "%d", &choice);
                if(choice == 1)
                    flag = fill_non_zero_vect(vector, n);
                else if(choice == 2)
                {
                    printf("Введите вектор в строку\n");
                    for (i = 0; i < *n-1 && flag; i++)
                    {
                        if (fscanf(stream, "%d ", &vector[i]) != 1)
                            flag = 0;
                    }
                    if (fscanf(stream, "%d", &vector[i]) != 1)
                        flag = 0;
                }
                else
                    printf("Некорректная команда\n");
            }
            else
            {    
                for (i = 0; i < *n-1 && flag; i++)
                {
                    if (fscanf(stream, "%d ", &vector[i]) != 1)
                        flag = 0;
                }
                if (fscanf(stream, "%d", &vector[i]) != 1)
                    flag = 0;
            }

            if (!flag)
            {
                if (stream == stdin)
                    for(; getchar() != '\n';);
                printf("Некорректные входные данные\n");
                vector = NULL;
                free(vector);
            }
        }
        else
            printf("Ошибка при выделении памяти\n");
    }
    else
    {
        if (stream == stdin)
            for(; getchar() != '\n';);
        printf("Некорректные входне данные\n");
    }

    return vector;
}


// Очистка памяти из-под матрицы
void free_matrix(int **matrix, int n)
{
    for (int i = 0; i < n; i++)
        free(matrix[i]);
    free(matrix);
}

// Ввод матрицы
int** input_matrix(int *n, int *m)
{
    int choice;
    int **matrix = NULL;
    int percent;
    char file_name[20];
    FILE *f_in;

    printf("Выберите способ ввода:\n");
    printf(" 1 - Ввод с клавиатуры в стандартном формате\n");
    printf(" 2 - Ввод с клавиатуры в разреженном формате\n");
    printf(" 3 - Ввод из файла\n");
    printf(" 4 - Автоматическая генерация\n");
    printf(" 0 - Выход в предыдущее меню\n");
    printf("Ваш выбор:   ");

    if (scanf("%d", &choice) != 1)
        choice = -1;

    switch (choice)
    {
        case 1:
            matrix = read_from_stream_matr(stdin, n, m);
            break;
        case 2:
            printf("Введите размеры матрицы (от 1 до %d )\n", MAX_COUNT);
            if (scanf("%d %d", n, m) != 2)
            {
                for(; getchar() != '\n';);
                printf("Некорректные входные данные\n");
            }
            else if (*n < 1 || *m < 1 || *n > MAX_COUNT || *m > MAX_COUNT)
                printf("Некорректные входные данные\n");
            else
            {
                matrix = read_spm_matr(*n, *m);
            }
            break;
        case 3:
            printf("Введите имя файла:   ");
            scanf("%s", file_name);
            f_in = fopen(file_name, "r");
            if (f_in)
            {
                matrix = read_from_stream_matr(f_in, n, m);
                fclose(f_in);
            }
            else
            {
                printf("Не удалось открыть файл\n");
                matrix = NULL;
            }
            break;
        case 4:
            if (input_sizes(stdin, n, m))
            {
                printf("Введите процент заполнения (от 1 до 100)\n");
                if (scanf("%d", &percent) != 1)
                {
                    for(;getchar() != '\n';);
                    printf("Некорректный ввод\n");
                }
                else if (percent < 1 || percent > 100)
                    printf("Недопустимое значение\n");
                else
                    matrix = generate_matr(*n, *m, percent);
            }
            break;
        case 0:
            return matrix;
        default:
            for(;getchar() != '\n';);
            printf("Некорректный ввод\n");
            break;
    }

    return matrix;
}

// Ввод вектора
int* input_vector(int *n)
{
    int choice;
    int *vector = NULL;
    int percent;
    char file_name[20];
    FILE *f_in;

    printf("Выберите способ ввода:\n");
    printf(" 0 - Ввод с клавиатуры в стандартном формате\n");
    printf(" 1 - Ввод с клавиатуры в разреженном формате\n");
    printf(" 2 - Ввод из файла\n");
    printf(" 3 - Автоматическая генерация\n");
    printf(" 4 - Выход в предыдущее меню\n");
    printf("Ваш выбор:   ");

    if (scanf("%d", &choice) != 1)
        choice = -1;

    switch (choice)
    {
        case 0:
            vector = read_from_stream_vect(stdin, n);
            break;
        case 1:
            printf("Введите размер вектора (от 1 до %d )\n", MAX_COUNT);
            if (scanf("%d", n) != 1)
            {
                for(; getchar() != '\n';);
                printf("Некорректные входне данные\n");
            }
            else if (*n < 1 || *n > MAX_COUNT)
                printf("Некорректные входне данные\n");
            else
            {
                vector = read_spm_vect(*n);
            }
            break;
        case 2:
            printf("Введите имя файла:   ");
            scanf("%s", file_name);
            f_in = fopen(file_name, "r");
            if (f_in)
            {
                vector = read_from_stream_vect(f_in, n);
                fclose(f_in);
            }
            else
            {
                printf("Не удалось открыть файл\n");
                vector = NULL;
            }
            break;
        case 3:
            printf("Введите размер вектора (от 1 до %d)\n", MAX_COUNT);
            if (scanf("%d", n) && *n > 0 && *n <= MAX_COUNT)
            {
                printf("Введите процент заполнения (от 1 до 100)\n");
                if (scanf("%d", &percent) != 1)
                {
                    for(;getchar() != '\n';);
                    printf("Некорректный ввод\n");
                }
                else if (percent < 1 || percent > 100)
                    printf("Недопустимое значение\n");
                else
                    vector = generate_vect(*n, percent);
            }
            else
            {
                for(;getchar() != '\n';);
                printf("Некорректное значение\n");
            }
            break;
        case 4:
            return vector;
        default:
            for(;getchar() != '\n';);
            printf("Некорректный ввод\n");
            break;
    }

    return vector;
}

// Печать матрицы
void print_matrix(int **matrix, int n, int m)
{
    if(matrix)
    {
        for(int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
                printf("%d ", matrix[i][j]);
            printf("\n");
        }
    }
}

// Генератор случайного целого числа
int rand_int(int a)
{
    return rand() / (double) RAND_MAX * a;
}

// Генерация случайной матрицы
int **generate_matr(int n, int m, int percent)
{
    int **matrix;
    int i, j, count;

    srand(time(NULL));

    matrix = allocate_matrix(n, m);

    if (matrix)
    {
        count = round(n*m* (double)percent/100);
        for (int k = 0; k < count; k++)
        {
            i = rand_int(n);
            j = rand_int(m);
            if (matrix[i][j] != 0)
                k--;
            else
                matrix[i][j] = rand_int(100);
        }
    }

    return matrix;
}

// Генерация случацного вектора
int *generate_vect(int n, int percent)
{
    int *vector = calloc(n, sizeof(int));
    int i, count;

    srand(time(NULL));

    if (vector)
    {
        count = round(n*(double)percent/100);
        for (int k = 0; k < count; k++)
        {
            i = rand_int(n);
            if (vector[i] != 0)
                k--;
            else
                vector[i] = rand_int(100);
        }
    }

    return vector;
}

// Стандартный алгоритм умножения
int *mult_stand(int **matrix, int *vector, int n, int r)
{
    int i, j;
    int *result;
    int sum;

    result = (int *)calloc(r, sizeof(int));
    if(result)
    {
        for(i = 0; i < r; i++)
        {
            sum = 0;
            for(j = 0; j < n; j++)
                sum += vector[j] * matrix[j][i];
            result[i] = sum;
        }
    }
    else
        result = NULL;

    return result;
}

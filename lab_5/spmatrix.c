#include "spmatrix.h"
#include "nmatrix.h"
#include <stdio.h>
#include <stdlib.h>

// Ввод матрицы в разреженном формате
int **read_spm_matr(int n, int m)
{
    int **matrix = NULL;
    int flag = 1, i = 0, j = 0, x = 0;
    int choice;

    matrix = allocate_matrix(n, m);
    if (matrix)
    {
        printf("Введи матрицу поэлементно в формате:\n<элемент> <индекс строки> <индекс столбца>\n");
        while (flag)
        {
            if (scanf("%d %d %d", &x,&i, &j) == 3 && (i >= 0) && (i < n) && (j >= 0) && (j < m))
            {
                matrix[i][j] = x;
                printf("Завершить ввод?\n0 - Нет, 1 - Да\n");

                if (scanf("%d", &choice) == 1)
                {
                    if (choice == 1)
                        flag = 0;
                    else
                        printf("Введите элемент\n");
                }
                else
                {
                    for (;getchar() != '\n';);
                    printf("Некорректная команда\n");
                }
            }
            else
            {
                for (;getchar() != '\n';);
                printf("Некорректные входные данные\n");
            }
        }
    }

    return matrix;
}

void free_spm(spm_t *spm_matr)
{
    node_t *tmp;

    free(spm_matr->A);
    free(spm_matr->IA);
    while (spm_matr->head)
    {
        tmp = spm_matr->head->next;
        free(spm_matr->head);
        spm_matr->head = tmp;
    }
}

void print_spm(spm_t spm_matr)
{
    int x = 0;
    node_t *tmp;

    tmp = spm_matr.head;
    while (tmp->next)
        tmp = tmp->next;
    x = tmp->index;
    if (spm_matr.A[0])
    {
        printf(" A:   ");
        for(int i = 1; i <= x; i++)
        {
            printf("%d ", spm_matr.A[i-1]);
        }
        printf("\n");

        printf(" IA:  ");
        for(int i = 1; i <= x; i++)
        {
            printf("%d ", spm_matr.IA[i-1]);
        }
        printf("\n");
        printf(" JA:  ");
        tmp = spm_matr.head;
        while(tmp->next)
        {
            printf("%d ", tmp->index);
            tmp = tmp->next;
        }
        printf("\n");
    }
    else
        printf("Нулевой вектор\n");
}

//перевод матрицы в разреженный формат
void convert(int **matrix, int n, int m, spm_t *spm_matrix)
{
    int i = 0, j = 0, k = 0, col = -1, x = k;
    spm_matrix->A = calloc(n*m, sizeof(int));
    spm_matrix->IA = calloc(n*m, sizeof(int));
    for(i = 0; i < m; i++)
    {
        x = 0;
        for(j = 0; j < n; j++)
        {
            if(matrix[j][i])
            {
                spm_matrix->A[k] = matrix[j][i];
                spm_matrix->IA[k] = j + 1;
                if (i > col)
                {
                    x = k + 1;
                    col = i;
                }
                k++;
            }
        }

        spm_matrix->head = push(spm_matrix->head, x);
    }
    spm_matrix->head = push(spm_matrix->head, k);
    spm_matrix->head = reverse(spm_matrix->head);
}

// Ввод вектора в разреженном формате
int *read_spm_vect(int n)
{
    int *vector = NULL;
    int flag = 1, i = 0, x = 0;
    int choice;

    vector = calloc(n, sizeof(int));
    if (vector)
    {
        printf("Введи вектор поэлементно в формате:\n<элемент> <столбцевой индекс>\n");
        while (flag)
        {
            if (scanf("%d %d", &x,&i) == 2 && (i >= 0) && (i < n))
            {
                vector[i] = x;
                printf("Завершить ввод?\n0 - Нет, 1 - Да\n");

                if (scanf("%d", &choice) == 1)
                {
                    if (choice == 1)
                        flag = 0;
                    else
                        printf("Введите элемент\n");
                }
                else
                {
                    for (;getchar() != '\n';);
                    printf("Некорректная команда\n");
                }
            }
            else
            {
                for (;getchar() != '\n';);
                printf("Некорректные входные данные\n");
            }
        }
    }

    return vector;
}

void mult_spm(spm_t spm_matr, spm_t spm_vect, spm_t *result, int n)
{
    int start, end, temp;
    node_t *tmp_1 = spm_matr.head, *tmp_2 = spm_vect.head;
    int sum = 0, k = 0, pos = 0, count = 0;
    int *IP = malloc(n * sizeof(int));

    for(int i = 0; spm_vect.head ->next; i++)
    {
        IP[i] = spm_vect.head->index;
        spm_vect.head = spm_vect.head->next;
    }

    start = spm_matr.head->index;

    while(tmp_1->next != NULL)
    {
        pos = 0;
        tmp_1 = tmp_1->next;
        end = tmp_1->index;
        if (!tmp_1->next)
                end++;

        if (start == 0)
            result->head = push(result->head, 0);
        else
        {
            count = 0;
            if (end == 0)
            {
                while(tmp_1->next && !tmp_1->index)
                {
                    count++;
                    tmp_1 = tmp_1->next;
                }
                end = tmp_1->index;
                if(!tmp_1->next)
                    end++;
            }

            sum = 0;
            for(int i = start; i < end; i++)
            {
                if(IP[spm_matr.IA[i-1]-1])
                    sum = sum + spm_vect.A[IP[spm_matr.IA[i-1]-1]-1] * spm_matr.A[i-1];
            }
            if(sum)
            {
                result->A[k] = sum;
                result->IA[k] = 1;
                result->head = push(result->head, k+1);
                k++;
            }
            else
                result->head = push(result->head, 0);
        }
        start = tmp_1->index;
    }
    result->head = push(result->head, k);
    result->head = reverse(result->head);
    free(IP);
}

node_t* push(node_t *head, int value)
{
    node_t *tmp = malloc(sizeof(node_t));

    if(tmp)
    {
        tmp->index = value;
        tmp->next = head;
        head = tmp;
        return head;
    }
    else
        return NULL;
}

node_t* reverse(node_t* head) 
{
    node_t* result;
    if(head->next == NULL)
        result = head;
    else
    {
        node_t* newhead = reverse(head->next);
        head->next->next = head;
        head->next = NULL;
        result = newhead;
    }
    return result;
}


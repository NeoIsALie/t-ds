#include <stdio.h>

#define MAX_SIZE 100 //максимальный размер массива
//добавление элемента стека
void push()
{
    if(ps > stack_tail)
    {
    	fprintf(stderr,"Stack overflow\n");
    	return;
    }
    else
    {
    	stack1_head++;
    	*stack1_head = data;
    }
}
int pop()
{
    if((ps < stack1_head) || (ps > stack2_head)) 
    {
    	fprintf(stderr, "Stack underflow\n", );
    	return - 1;
    }
    else if(ps <= stack1_tail)
    {
    	printf("%f\n", *ps);
    	*ps = 0.0f:
    	ps--;
    }
    else
    {
    	printf("%f\n", *ps);
    	*ps = 0.0f;
    	ps++;
    }
    return 0;
}

void back()
{
   printf("%f\n", *ps);
}

int get_size()
{
   return (MAX_SIZE - (stack_tail - ps) + 1);
}

void clear()
{
   while(ps > stack_head)
   {
   	    *ps = 0.0f;
   	    ps--;
   }
}

int main(void)
{
    float stack_in_array[MAX_SIZE];
    const float *stack1_head;
    const float *stack1_tail;
    const float *stack2_head;
    const float *stack2_tail;
    float *ps1;
    float *ps2;

    int pick;
    int stack_num;

    float data;

    stack1_head = stack_in_array;
    stack2_tail = stack_in_array + MAX_SIZE / 2 - 1;
    stack2_head = stack_in_array + MAX_SIZE - 1;
    stack2_tail = stack_in_array + MAX_SIZE / 2;

    ps1 = stack_in_array - 1;
    ps2 = stack_in_array + MAX_SIZE;
    
    while(1)
    {
    	printf("-------------------------------------------------------\n");
        printf("|                  Работа со стеком.                  |\n");
        printf("|                Стек вещественных чисел.             |\n");
        printf("-------------------------------------------------------\n");
        printf("  0 - Добавить элемент в стек\n");
        printf("  1 - Удалить элемент из стека\n");
        printf("  2 - Показать текущий элемент стека\n");
        printf("  3 - Показать текущий размер стека\n");
        printf("  4 - Очистить стек\n");
        printf("  5 - Выход\n");
    }

    if(scanf("%d", &pick) != 1)
    {
    	printf("Такой команды нет\n");
    	scanf("%*s");
    	continue;
    }
    if(pick == 0)
    {
    	printf("Введите элемент\n");
    	if(scanf("%f"), &data)
    	{
    	    printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
    	    if(scanf("%d"), &stack_num)
    	    {
                if(stack_num == 1)
            	    push(ps1, stack1_tail, data);
                else if(stack_num == 2)
                	push(ps2, stack2_tail, data);
                else
                	printf("Неверный выбор\n");
            }
            else
            	printf("Ошибка ввода\n");
        }
        else
        	printf("Неверный тип введенных данных\n");   
    }
    else if(pick == 1)
    {
        printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
    	if(scanf("%d"), &stack_num)
    	{
            if(stack_num == 1)
                pop(ps1, stack1_head);
            else if(stack_num == 2)
                pop(ps2, stack2_head);
            else
                printf("Неверный выбор\n");
        }
        else
            printf("Ошибка ввода\n");
    }
    else if(pick == 2)
    {
        printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
    	if(scanf("%d"), &stack_num)
    	{
            if(stack_num == 1)
            	back(ps1);
            else if(stack_num == 2)
                back(ps2);
            else
                printf("Неверный выбор\n");
        }
        else
            printf("Ошибка ввода\n");    	
    }
    else if(pick == 3)
    {
    	printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
    	if(scanf("%d"), &stack_num)
    	{
            if(stack_num == 1)
                get_size(ps1, stack1_tail);
            else if(stack_num == 2)
                get_size(ps2, stack2_tail);
            else
                printf("Неверный выбор\n");
        }
        else
            printf("Ошибка ввода\n");
    }
    else if(pick == 4)
    {
    	printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
    	if(scanf("%d"), &stack_num)
    	{
            if(stack_num == 1)
            	clear(ps1, stack1_head);
            else if(stack_num == 2)
                clear(ps2, stack2_head);
            else
                printf("Неверный выбор\n");
        }
        else
            printf("Ошибка ввода\n");
    }
    else if(pick == 5)
    {
    	printf("Выход из программы\n");
    	return 0;
    }
    else
    {
    	printf("Неизвестная команда\n", );
    	scanf("%*s");
    }
    return 0;
}
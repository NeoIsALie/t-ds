#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	float value;
	struct node *next;
}node;

void push(node **head, int data) {
    node *tmp = (Node*) malloc(sizeof(Node));
    tmp->value = data;
    tmp->next = (*head);
    (*head) = tmp;
}

int pop(node **head) 
{
    node* prev = NULL;
    int val;
    if (head == NULL) 
    {
    	printf("Stack underflow\n");
        return -1;
    }
    prev = (*head);
    val = prev->value;
    (*head) = (*head)->next;
    free(prev);
    return val;
}

node* back(node *head) 
{
    if (head == NULL) 
        return NULL;
    while (head->next)
        head = head->next;
    return head;
}

int get_size(node *head)
{
    int size = 0;
    while(*(head) != NULL)
    	size++;
    return size;
}

void clear(node **head) 
{
    while ((*head)->next) 
    {
        pop(head);
        *head = (*head)->next;
    }
    free(*head);
}

int main(void)
{
	node *head = NULL;
	int pick;
	float data;

	while(1)
    {
    	printf("-------------------------------------------------------\n");
        printf("|                  Работа со стеком.                  |\n");
        printf("|                Стек вещественных чисел.             |\n");
        printf("-------------------------------------------------------\n");
        printf("  0 - Добавить элемент в стек\n");
        printf("  1 - Удалить элемент из стека\n");
        printf("  2 - Показать текущий элемент стека\n");
        printf("  3 - Показать текущий размер стека\n");
        printf("  4 - Очистить стек\n");
        printf("  5 - Выход\n");
    }

    if(scanf("%d", &pick) != 1)
    {
    	printf("Неверный ввод\n");
    	scanf("%*s");
    	continue;
    }

    if(pick == 0)
    {
    	printf("Введите элемент\n");
    	if(scanf("%f", data))
    		push(&head, data);
    	else
    		printf("Неверный тип данных\n");
    }
    else if(pick == 1)
    {
    	int result = pop(&head);
    	printf("%f\n", result);
    }
    else if(pick == 2)
    {
    	node *elem = back(head);
    	printf("%f\n", *elem.value);
    }
    else if(pick == 3)
    {
    	int size = get_size(head);
    	printf("%d\n",size);
    }
    else if(pick == 4)
    	clear(&head);
    else if(pick == 5)
    {
    	printf("Выход из программы\n");
    	return 0;
    }
    else
    {
    	printf("Такой команды нет\n");
    	scanf("%*s");
    }

    return 0;
}
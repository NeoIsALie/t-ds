#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdlib.h> 

typedef struct Node
{
    float value;
    struct node *next;
} node;

void push2(node **head, float data);
int pop2(node **head);
node* back2(node *head);
void print_stack(node *head);
int main_ll(void);

#endif
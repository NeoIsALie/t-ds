#ifndef ARRAY_H
#define ARRAY_H

#include <stdio.h>

#define MAX_SIZE 4 //максимальный размер массива

float* push1(float *ps, float *stack1_head, float *stack2_head, float *stack1_tail, float *stack2_tail, float data);
float* pop1(float *ps, float *stack1_head, float *stack2_head, float *stack1_tail);
void back1(float *ps, float *stack1_head, float *stack2_head);
void print1_stack(float *stack_head, float *stack1_head, float *stack2_head, float *ps);
int main_array(void);

#endif
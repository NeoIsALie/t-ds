#include "array.h"
#include "linkedlist.h"
#include <inttypes.h>
#include <time.h>
#include "timetest.h"

void print_main_menu()
{
    printf("Выберите вариант реализации стека\n");
    printf("-------------------------------------------------------\n");
    printf("|1 - массив, 2 - список                                |\n");
    printf("|3 - сравнение реализаций стека, 4 - выход из программы|\n");
    printf("-------------------------------------------------------\n");
    printf("Выбор: ");
}

int main(void)
{
    setbuf(stdout, NULL);
    char buffer[128];
    int choice;
    while(1)
    {
        print_main_menu();
        fgets(buffer, 128, stdin);
        while(buffer[0] == '\n')
        {
            printf("Пустой ввод\n\n\n");
            print_main_menu();
            fgets(buffer, 128, stdin);
        }
        choice = atoi(buffer);
        if(choice == 1)
            main_array();
        else if(choice == 2)
            main_ll();
        else if(choice == 3)
            timetest();
        else if(choice == 4)
        {
            printf("Выход из программы\n");
            return 0;
        }
        else
            printf("Такой команды нет\n\n\n");
    }
    return 0;
}
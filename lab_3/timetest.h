#ifndef TIMETEST_H
#define TIMETEST_H

int64_t tick(void);
float *push_array(float *ps, float *stack_tail, float data);
float* pop_array(float *ps);
void push_list(node **head, float data);
int pop_list(node **head);
void test_array(int tests, float* stack_head, float* stack_tail, float* ps);
void test_list(int tests, node *head);
int timetest(void);

#endif
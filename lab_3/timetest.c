#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

int64_t tick(void)
{
    int64_t d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d));
    return d;
}

float *push_array(float *ps, float *stack_tail, float data)
{
    if(ps < stack_tail)
    {
        ps++;
        *ps = data;
    }
    return ps;
}

float* pop_array(float *ps)
{
    *ps = 0.0f;
    ps--;
    return ps;
}

typedef struct Node
{
    float value;
    struct node *next;
} node;


void push_list(node **head, float data) 
{
    node *tmp = (node*) malloc(sizeof(node));
    tmp->value = data;
    tmp->next = (*head);
    (*head) = tmp;
}

// удаление элемента из стека
int pop_list(node **head) 
{
    node* prev = NULL;
    float val;
    if (*head == NULL) 
    {
        printf("Ошибка: стек пуст\n");
        return -1;
    }
    prev = (*head);
    val = prev->value;
    (*head) = (*head)->next;
    free(prev);
    return 0;
}

void test_array(int tests, float* stack_head, float* stack_tail, float* ps)
{
    int64_t time1;
    int64_t time2;
    float a = 5.0;
    float temp;
    
    time1 = tick();
    for(int i = 0; i < tests; i++)
    {
        temp = ((float)rand()/(float)(RAND_MAX)) * a;
        ps = push_array(ps,stack_tail, temp);
    }
    for(int i = 0; i < tests; i++)
    {
        ps = pop_array(ps);
    }
    time2 = tick();

    int64_t result = (time2 - time1)/CLOCKS_PER_SEC;
    printf(" %21d  ", tests);
    printf("%15c" "%" PRId64 " " "%9c", ' ',result, ' ');

}

void test_list(int tests, node *head)
{
    int64_t time1, time2;
    float temp, a = 5.0;
    time1 = tick();
    for(int i = 0; i < tests; i++)
    {
        temp = ((float)rand()/(float)(RAND_MAX)) * a;
        push_list(&head, temp);
    }

    for(int i = 0; i < tests; i++)
    {
        pop_list(&head);
    }
    time2 = tick();

    int64_t result = (time2 - time1)/CLOCKS_PER_SEC;
    if(result > 100 && result < 1000)
        printf(" %14c" "%" PRId64 " " "%18c", ' ', result, ' ');
    else if(result > 1000)
        printf(" %13c" "%" PRId64 " " "%18c", ' ', result, ' ');
    else
        printf(" %15c" "%" PRId64 " " "%18c", ' ', result, ' ');

}
int timetest(void)
{
    float stack_in_array[5000];
    float *stack_head;
    float *stack_tail;
    float *ps;

    stack_head = stack_in_array;
    stack_tail = stack_in_array + 4999;
    ps = stack_head - 1;

    node *head = NULL;
    printf("---------------------------------------------------------------------------------------\n");
    printf("| Количество элементов | Время работы стека-массива, мс | Время работы стека-списка, мс|\n");
    printf("---------------------------------------------------------------------------------------\n");
    test_array(5, stack_head, stack_tail, ps);
    test_list(5, head);
    printf("\n");
    test_array(50, stack_head, stack_tail, ps);
    test_list(50, head);
    printf("\n");
    test_array(100, stack_head, stack_tail, ps);
    test_list(100, head);
    printf("\n");
    test_array(1000, stack_head, stack_tail, ps);
    test_list(1000, head);
    printf("\n");
    test_array(5000, stack_head, stack_tail, ps);
    test_list(5000, head);
    printf("\n");
    printf("---------------------------------------------------------------------------------------\n");

    return 0;
}
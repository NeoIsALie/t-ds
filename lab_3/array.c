#include "array.h"
#include <stdlib.h>
//добавление элемента в стек
float* push1(float *ps, float *stack1_head, float *stack2_head, float *stack1_tail, float *stack2_tail, float data)
{
    if(ps < stack1_tail)
    {
        ps++;
        *ps = data;
        printf("Элемент добавлен в стек\n");
    }
    else if(ps > stack2_tail)
    {
        ps--;
        *ps = data;
        printf("Элемент добавлен в стек\n");
    }
    else if(ps > stack1_tail)
    {
        printf("Переполнение стека \n");
        return ps;
    }
    else if(ps < stack2_tail)
    {
        printf("Переполнение стека \n");
        return ps;
    }
    return ps;
}
// удаление элемента из стека
float* pop1(float *ps, float *stack1_head, float *stack2_head, float *stack1_tail)
{
    if(ps < stack1_head)
        printf("Ошибка: стек пуст\n");
    else if (ps > stack2_head)
        printf("Ошибка: стек пуст\n");
    else if(ps <= stack1_tail)
    {
        printf("%f\n", *ps);
        *ps = 0.0f;
        ps--;
    }
    else
    {
        printf("%f\n", *ps);
        *ps = 0.0f;
        ps++;
    }
    return ps;
}
// возвращение последнего элемента стека
void back1(float *ps, float *stack1_head, float *stack2_head)
{
    if((ps < stack1_head) || (ps > stack2_head))
       printf("Ошибка: стек пуст\n");
    else
       printf("%f\n", *ps);
}

// печать стека 
void print1_stack(float *stack_head, float *stack1_head, float *stack2_head, float *ps)
{
    if(ps < stack1_head || ps > stack2_head)
    {
        printf("Стек:\n");  
    }
    else if(stack_head <= ps)
    {   
        printf("Стек:\n");
        while(stack_head <= ps)
        {
            printf("%f\n", *stack_head);
            stack_head++;
        }
    }
    else if(stack_head >= ps)
    {
        printf("Стек:\n");
        while(ps <= stack_head)
        {
            printf("%f\n", *ps);
            ps++;
        }
    }
}

// void clear1(float *ps, float *stack_head)
// {
//    while(ps > stack_head)
//    {
//          *ps = 0.0f;
//          ps--;
//    }
// }

int main_array(void)
{
    setbuf(stdout, NULL);
    char buf[128];
    int res;
    float stack_in_array[MAX_SIZE];
    float *stack1_head;
    float *stack1_tail;
    float *stack2_head;
    float *stack2_tail;
    float *ps1;
    float *ps2;

    int size1 = 0;
    int size2 = 0;

    char stack_num;

    float data;

    stack1_head = stack_in_array;
    stack1_tail = stack_in_array;
    stack2_head = stack_in_array + MAX_SIZE - 1;
    stack2_tail = stack_in_array + MAX_SIZE - 1;

    ps1 = stack1_head - 1;
    ps2 = stack2_head + 1;
    
    while(1)
    {
        printf("-------------------------------------------------------\n");
        printf("|                  Работа со стеком.                  |\n");
        printf("|        Стек вещественных чисел (массив).            |\n");
        printf("-------------------------------------------------------\n");
        printf("  1 - Добавить элемент в стек\n");
        printf("  2 - Удалить элемент из стека\n");
        printf("  3 - Показать текущий элемент стека\n");
        printf("  4 - Вывести стек");
        printf("  5 - Выход\n");
        printf("-------------------------------------------------------\n");
        printf("Выбор: ");
        fgets(buf, 128, stdin);
        while(buf[0] == '\n')
        {
            printf("Пустой ввод\n");
            printf("Выбор: ");
            fgets(buf, 128, stdin);
        }
        res = atoi(buf);
        if(res < 1 || res > 5)
        {
            printf("Такой команды нет\n");
            continue;
        }
        if(res == 1)
        {
            printf("Введите элемент\n");
            if(scanf("%f", &data))
            {
                printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
                getchar();
                fgets(buf, 128, stdin);
                while(buf[0] == '\n')
                {
                    printf("Пустой ввод\n");
                    printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
                    fgets(buf, 128, stdin);
                }
                stack_num = atoi(buf);
                if(stack_num == 1)
                {
                    printf("%d\n", MAX_SIZE - size2);
                    if(size1 + size2 < MAX_SIZE)
                    {   
                        ps1 = push1(ps1, stack1_head, stack2_head, stack1_tail, stack2_tail, data);
                        stack1_tail++;
                        size1++;
                    }
                    else
                        printf("Переполнение стека 1\n\n");
                }
                else if(stack_num == 2)
                {
                    printf("%d\n", MAX_SIZE - size1);
                    if(size2 + size1 < MAX_SIZE)
                    {
                        size2++;
                        ps2 = push1(ps2, stack1_head, stack2_head, stack1_tail, stack2_tail, data);
                        stack2_tail--;
                    }
                    else
                        printf("Переполнение стека 2\n\n");
                }
                else
                {
                    printf("Неверный выбор\n");
                }
            }
            else
            {
                printf("Неверный тип введенных данных\n"); 
                getchar();
                getchar();
            }
        }
        else if(res == 2)
        {
            printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
            fgets(buf,128,stdin);
            while(buf[0] == '\n')
            {
                printf("Пустой ввод\n");
                printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
                fgets(buf, 128, stdin);
            }
            stack_num = atoi(buf);
            if(stack_num == 1)
            {
                if(size1 > 0)
                {   
                    ps1 = pop1(ps1, stack1_head, stack2_head, stack1_tail);
                    size1--;
                    stack1_tail--;
                    printf("size1 = %d", size1);
                }
                else
                    printf("Ошибка: стек пуст\n");
            }
            else if(stack_num == 2)
            {
                if(size2 > 0)
                {
                    ps2 = pop1(ps2, stack1_head, stack2_head, stack1_tail);
                    size2--;
                    stack2_tail++;
                    printf("size2 = %d", size2);
                }
                else
                    printf("Ошибка: стек пуст\n");
            }
            else
                {
                    printf("Неверный выбор/Ошибка ввода\n");
                    scanf("%*c");
                }
        }
        else if(res == 3)
        {
            printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
            fgets(buf,128,stdin);
            while(buf[0] == '\n')
            {
                printf("Пустой ввод\n");
                printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
                fgets(buf, 128, stdin);
            }
            stack_num = atoi(buf);
            if(stack_num == 1)
                back1(ps1,stack1_head,stack2_head);
            else if(stack_num == 2)
                back1(ps2, stack1_head, stack2_head);
            else
            {
                printf("Неверный выбор/Ошибка ввода\n");
                scanf("%*c");
            }
        }
        else if(res == 4)
        {
            printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
            fgets(buf,128,stdin);
            while(buf[0] == '\n')
            {
                printf("Пустой ввод\n");
                printf("Выберите стек: 1 - первый стек, 2 - второй стек\n");
                fgets(buf, 128, stdin);
            }
            stack_num = atoi(buf);
            if(stack_num == 1)
                print1_stack(stack1_head, stack1_head, stack2_head, ps1);
            else if(stack_num == 2)
                print1_stack(stack2_head, stack1_head, stack2_head, ps2);
            else
            {
                printf("Неверный выбор/Ошибка ввода\n");
                scanf("%*c");
            }
        }
        else if(res == 5)
        {
            printf("Выход во внешнее меню\n\n\n");
            return 0;
        }
        else
        {
            printf("Неизвестная команда\n");
            printf("-------------------------------------------------------\n");
            scanf("%*c");
        }
    }
    return 0;
}

#include "linkedlist.h"
#include <stdio.h>

#define MAX_SIZE 5
//добавление элемента в стек
void push2(node **head, float data) 
{
    node *tmp = (node*) malloc(sizeof(node));
    tmp->value = data;
    tmp->next = (*head);
    (*head) = tmp;
}

// удаление элемента из стека
int pop2(node **head) 
{
    node* prev = NULL;
    float val;
    if (*head == NULL) 
    {
        printf("Ошибка: стек пуст\n");
        return -1;
    }
    prev = (*head);
    val = prev->value;
    (*head) = (*head)->next;
    free(prev);
    printf("%f\n", val);
    return 0;
}

//печать стека
void print_stack(node *head)
{
    printf("Список:\n");
    node *current = head;
    while(current)
    {
        printf("%f\n", current->value);
        current = current->next;
    }
}

// возвращение последнего элемента стека
node* back2(node *head) 
{
    if(head == NULL) 
    {
        printf("Ошибка: стек пуст\n");
        return NULL;
    }
    while (head->next)
        head = head->next;
    return head;
}

// void clear2(node **head)
// {
//  node *current = *head;
//  node *next;
//  while (current) 
//  {
//      next = current->next;
//      free(current);
//      current = next;
//  }
//  *head = NULL;
// }

int main_ll(void)
{
    char buf2[128];

    int size = 0;
    int n = 0;
    int pick;

    node *head = NULL;
    node *array_free[1000];

    float data;
    while(1)
    {
        printf("-------------------------------------------------------\n");
        printf("|                  Работа со стеком.                  |\n");
        printf("|       Стек вещественных чисел (список).             |\n");
        printf("-------------------------------------------------------\n");
        printf("  1 - Добавить элемент в стек\n");
        printf("  2 - Удалить элемент из стека\n");
        printf("  3 - Показать текущий элемент стека\n");
        printf("  4 - Вывести стек\n");
        printf("  5 - Выход\n");
        printf("-------------------------------------------------------\n");
        printf("Выбор: ");
        fgets(buf2, 128, stdin);
        while(buf2[0] == '\n')
        {
            printf("Пустой ввод\n\n\n");
            printf("Выбор: ");
            fgets(buf2, 128, stdin);
        }
        pick = atoi(buf2);
        if(pick < 1 || pick > 5)
        {
            printf("Такой команды нет\n\n\n");
            continue;
        }
        if(pick == 1)
        {
            printf("Введите элемент\n");
            if(scanf("%f", &data))
            {
                getchar();
                if(size >= MAX_SIZE)
                {
                    printf("Переполнение стека\n");
                }
                else
                {
                    size++;
                    push2(&head, data);
                    printf("Элемент добавлен в стек\n\n");
                    for(int i = 0; i < n; i++)
                    {
                        if (head == array_free[i])
                        {
                            n--;
                            for(int j = i; j < n; j++)
                            {
                                array_free[j] = array_free[j + 1];
                            }
                            break;
                        }
                    }
                }
            }
            else
            {
                printf("Неверный тип введенных данных\n");
                scanf("%*c");
            }
        }
        else if(pick == 2)
        {
            if (head != NULL)
            {
                array_free[n++] = head;
            }
            if(pop2(&head) != -1)
            {
                printf("Элемент удален из стека\n\n");
                size--;
            }
        }
        else if(pick == 3)
        {
            node *elem = back2(head);
            if(elem != NULL)
                printf("%f\n", elem->value);
        }
        else if(pick == 4)
        {
            print_stack(head);
            printf("Свободная память:\n");
            for(int i = 0; i < n; i++)
            {
                printf("%p\n", array_free[i]);
            }
        }
        // else if(pick == 4)
        // {
        //     node *current = head;
        //     while(current)
        //     {
        //         array_free[n++] = current;
        //         current = current->next;
        //     }
        //     clear2(&head);
        // }
            
        else if(pick == 5)
        { 
            printf("Выход во внешнее меню\n\n\n");
            return 0;
        }
        else
        {
            printf("Такой команды нет\n");
        }
    }
    return 0;
}
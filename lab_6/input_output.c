#include "input_output.h"

int check_word(char *word)  // проверка является ли строка словом
{
	int rn = OK;
	while(*word && *word != '\n' && !rn)
	{
		if ((*word <'A' || *word >'Z') && (*word <'a' || *word >'z'))
			rn = NOT_WORD;
		word++;
	}

	return rn;
}

void strcopy(char *str_1, char *str_2)  // копирование строки
{
	while(*str_2 && *str_2 != '\n')
	{
		*str_1 = *str_2;
		str_1++;
		str_2++;
	}
	*str_1 = '\0';
}

tree_t* read_tree(FILE *f, int *ret)  // построение дерева
{
	tree_t *root = NULL;  // корень дерева
	tree_t *tmp = NULL;  // элемент дерева
	char *string = (char*)calloc(MAX_LEN +2,sizeof(char));  // строка файла

	*ret = OK;
	while(fgets(string,MAX_LEN + 2,f) && !*ret)
	{
		if(*string != '\n')
		{
			tmp = (tree_t*)malloc(sizeof(tree_t));
			if (tmp)
			{
				*ret = check_word(string);
				if (!*ret)
				{
					strcopy(tmp->word, string);
					tmp->left = NULL;
					tmp->right = NULL;
					root = add_tree(root, tmp);
				}
			}
			else
				*ret = ERR_MEMORY;
		}
	}
	free(string);
	return root;
}

void output_tree(tree_t *root, int deep, char side)  // вывод дерева
{
	if (!root)
			return;
	printf("|");
	for (int i = 0; i < deep - 1; i++)
		printf("--");
	if (deep > 1)
		printf("|--");
	else if (deep == 1)
		printf("--");


	printf("%s",root->word);
	if (side == 'l')
		printf("  // лево\n");
	if (side == 'r')
		printf("  // право\n");
	if (side == 'm')
		printf("\n");
	if (root->right)
		output_tree(root->right,deep+1,'r');
	if (root->left)
		output_tree(root->left,deep+1, 'l');
	return;

}

tree_t* input_word(tree_t *root, int *ret)  // добавление элемента
{
	char *string = (char*)calloc(MAX_LEN +2,sizeof(char));  // слово
	tree_t *tmp = (tree_t*)malloc(sizeof(tree_t));

	printf("    Введите слово: ");
	scanf("%s",string);
	printf("\n");
	if (tmp)
	{
		*ret = check_word(string);
		if (!*ret)
		{
			strcopy(tmp->word, string);
			tmp->left = NULL;
			tmp->right = NULL;
			root = add_tree(root, tmp);
		}
	}
	else
		*ret = ERR_MEMORY;
	free(string);
	return root;
}

void apply(tree_t *root, void (*f)(tree_t*, void*, char), void *arg, char letter)  // рекурсивная печать в файл
{
	if (root == NULL)
		return;

	f(root, arg, letter);
	apply(root->left, f, arg, letter);
	apply(root->right, f, arg, letter);
}

void to_dot(tree_t *root, void *param, char letter)  // запись в dot
{
	FILE *f = param;

	if (root->left)
		fprintf(f, "%s -> %s;\n", root->word, root->left->word);

	if (root->right)
		fprintf(f, "%s -> %s;\n", root->word, root->right->word);
}

void color_in(tree_t *root, void *param, char letter)  // раскрашивание вершин
{
	FILE *f = param;

	if (root)
	{
		if (root->word[0] == letter)
			fprintf(f, "%s[style = filled, fillcolor = green]\n", root->word);
	}

}

void export_to_dot(FILE *f, const char *tree_name, tree_t *root, int color, char letter)  // экспорт графа
{
	fprintf(f, "digraph %s {\n", tree_name);

	apply(root, to_dot, f, letter);

	if (color)
		apply(root, color_in, f, letter);

	fprintf(f, "}\n");
}

void free_tree(tree_t *root)  // освобождение памяти
{
	tree_t *left, *right;
	if(!root)
		return;

	right = root->right;
	left = root->left;
	free(root);
	if (left)
		free_tree(left);

	if (right)
		free_tree(right);
}

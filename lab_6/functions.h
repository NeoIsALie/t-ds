#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__

#include "define_file.h"
#include "input_output.h"
#include <time.h>
#include <inttypes.h>

tree_t* add_tree(tree_t *root, tree_t *add_word);  // добавление слова в дерево

void postfix(tree_t *root);  // постфиксный обход дерева
void prefix(tree_t *root);  // префиксный обход дерева
void infix(tree_t *root);  // инфиксный обход дерева


void remove_word(tree_t **root,char* search);  // удаление слова из дерева

void find_words(tree_t *root,char letter, int *count);  // поиск слова на нужную букву

tree_t* search_word(tree_t *root, char* search);  // поиск узлов

void time_it();  // замер времени выполнения

#endif

#include "define_file.h"
#include "functions.h"
#include "input_output.h"

int main()
{
	setbuf(stdout, NULL);
	int rn = OK;
	int choice = -1;
	int exist = 0;
	tree_t *tree = NULL;
	FILE *f_in;
	char *file_name = (char*)calloc(MAX_LEN+2,sizeof(char));
	int deep;
	char *search = (char*)calloc(MAX_LEN+2,sizeof(char));
	char letter;
	int count;
	while(choice)
	{
		printf("Работа с деревом. Действия:\n\
1 - построение дерева\n\
2 - вывод дерева на экран\n\
3 - обход дерева\n\
4 - добавление слова в дерево\n\
5 - удаление слова из дерева\n\
6 - поиск слова в дереве\n\
7 - найти вершины, начинающиеся на нужную букву\n\
8 - сравнить время поиска в дереве и файле\n\
0 - выход из программы\n\n");
		printf("Ваш выбор: ");
		if (scanf("%d",&choice) != 1)
		{
			printf("Введен некорректный символ\n\n");
			rn = WRONG_SYMB;
			break;
		}
		printf("\n");
		switch (choice)
		{
			case 1: printf("    Введите название файла: ");
					scanf("%s",file_name);
					printf("\n");
					f_in = fopen(file_name, "r");
					if (f_in)
					{
						if (exist)
						{
							free_tree(tree);
							tree = NULL;
							exist = 0;
						}
						tree = read_tree(f_in, &rn);
						if (!rn)
						{
							printf("    Данные введены корректно\n\n");
							exist = 1;
						}
						else if (rn == NOT_WORD)
							printf("    ОШИБКА: в файле записано не слово, данные не введены\n\n");
						else
							printf("    Произошла ошибка при выделении памяти, данные не введены\n\n");
						fclose(f_in);
					}
					else
					{
						printf("    ОШИБКА: файл '%s' не найден\n\n",file_name);
						rn = CANNOT_OPEN;
					}

				 break;
			case 2: if (exist)
					{
						int ch = -1;
						FILE *f_out;
						printf("    Выберете способ вывода:\n");
						printf("    1 - в консоли       2 - в файле\n");
						printf("    Ваш выбор: ");
						if (scanf("%d",&ch) != 1)
						{
							printf("    Введен некорректный символ\n\n");
							rn = WRONG_SYMB;
						}
						else
						{
							switch(ch)
							{
								case(1): deep = 0;
										 printf("\n    Дерево: \n");
										 output_tree(tree, deep, 'm');
										 printf("\n");
										 break;
								case(2): f_out = fopen("tree.gv", "w");
										 export_to_dot(f_out, "Tree", tree, 0, '\n');
										 fclose(f_out);
										 system("dot -Tpng tree.gv -otree.png");
										 system("shotwell tree.png");
										 printf("\n");
										 break;
								default: printf("   Введен неизвестный пункт меню\n\n");
										 break;
						   }

						}
					}
					else
						printf("    Данные еще не введены\n\n");

				break;
			case 3: if (exist)
					{
						printf("    Префиксный обход дерева\n");
						prefix(tree);
						printf("\n\n");
						printf("    Инфиксный обход дерева\n");
						infix(tree);
						printf("\n\n");
						printf("    Постфиксный обход дерева\n");
						postfix(tree);
						printf("\n\n");
					}
					else
						printf("    Данные еще не введены\n\n");
				break;
			case 4:tree = input_word(tree, &rn);
				   if (!rn)
				   {
					   printf("    Добавление выполнено успешно\n\n");
					   exist = 1;
				   }
				   else if (rn == NOT_WORD)
					   printf("    ОШИБКА: введено не слово\n\n");
				   else
					   printf("    Произошла ошибка при выделении памяти, данные не введены\n\n");
				break;
			case 5: if (exist)
					{
						printf("    Введите слово для удаления: ");
						scanf("%s",search);
						printf("\n");
						rn = check_word(search);
						if(!rn)
						{
							if (search_word(tree, search))
							{
								remove_word(&tree, search);
								if (tree)
									printf("    Слово успешно удалено\n\n");
								else
								{
									printf("    Дерево пусто\n\n");
									exist = 0;
								}
							}
							else
								printf("    Слова '%s' нет в дереве\n\n",search);
						}
						else
							printf("    ОШИБКА: введено не слово\n\n");
					}
					else
						printf("    Данные еще не введены\n\n");
				break;
			case 6: if (exist)
					{
					   printf("    Введите слово: ");
					   scanf("%s",search);
					   printf("\n");
					   rn = check_word(search);
					   if(rn)
					   {
						  printf("    Введено недопустимое значение\n\n");
						  rn = NOT_WORD;
					   }
					   else
					   {

						   if (search_word(tree, search))
							   printf("    В дереве присутствует слово '%s'\n\n",search);
						   else
							   printf("    Слово '%s' не найдено\n\n",search);
					   }
					}
					else
						printf("    Данные еще не введены\n\n");

				break;
			case 7: if (exist)
					{
						printf("    Введите букву: ");
						letter = getchar();
						letter = getchar();
						if (letter != '\n')
						{
							if ((letter <'A' || letter >'Z') && (letter <'a' || letter >'z'))
							{
							   printf("    Введено недопустимое значение\n\n");
							   rn = NOT_WORD;
							}
							else
							{
								count = 0;
								find_words(tree, letter, &count);
								if (!count)
									printf("    Слов на букву '%c' не найдено\n\n",letter);
								else
								{
									printf("    Число слов, начинающихся на букву '%c': %d\n",letter,count);
									FILE *f_out = fopen("tree_color.gv", "w");
									export_to_dot(f_out, "Tree", tree, 1, letter);
									fclose(f_out);
									system("dot -Tpng tree_color.gv -otree_color.png");
									system("shotwell tree_color.png");
									printf("\n");
								}
							}
						}
						else
						{
							printf("    Вы ничего не ввели\n\n");
							rn = NO_INPUT;
						}
					}
					else
						printf("    Данные еще не введены\n\n");
				break;
			case 8:time_it();
				break;
			case 0: printf("Выход из программы.\n\n");
					if(exist)
						free_tree(tree);
				   if (file_name)
					   free(file_name);
				   if (search)
					   free(search);
				break;
			default: printf("Неверный пункт меню\n\n");
				break;
		}
	}

	return rn;

}

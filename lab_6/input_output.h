#ifndef __INPUT_OUTPUT_H__
#define __INPUT_OUTPUT_H__

#include "define_file.h"
#include "functions.h"

tree_t* read_tree(FILE *f, int *ret);  // построение дерева

void output_tree(tree_t *root, int deep, char side);  // вывод дерева

tree_t* input_word(tree_t *root, int *ret);  // добавление элемента

void export_to_dot(FILE *f,const char *tree_name, tree_t *root, int color, char letter);  // вывод дерева в файл

void free_tree(tree_t *root);  // освобождение памяти
void strcopy(char *str_1, char *str_2);  // копирование строки
int check_word(char *word);  // проверка является ли строка словом

#endif


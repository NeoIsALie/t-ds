#include "functions.h"

int64_t tick(void)
{
	int64_t d;
	__asm__ __volatile__ ("rdtsc" : "=A" (d));
	return d;
}

tree_t* add_tree(tree_t *root, tree_t *add_word)  // добавление элемента в дерево
{
	if (root != NULL)
	{
		if (strcmp(root->word, add_word->word) > 0)
			root->right = add_tree(root->right, add_word);
		else if (strcmp(root->word, add_word->word) < 0)
			root->left = add_tree(root->left, add_word);
	}
	else
		root = add_word;

	return root;
}

void postfix(tree_t *root)  // постфиксный обход дерева
{
	if (!root)
		return;
	postfix(root->left);
	postfix(root->right);
	printf("%s   ",root->word);
	return;
}

void prefix(tree_t *root)  // прекфиксный обход дерева
{
	if (!root)
		return;
	printf("%s   ",root->word);
	prefix(root->left);
	prefix(root->right);
	return;
}

void infix(tree_t *root)  // инфиксный обход дерева
{
	if (!root)
		return;
	infix(root->left);
	printf("%s   ",root->word);
	infix(root->right);
	return;
}

void find_replace(tree_t **branch, tree_t *root)  // поиск самого правого потомка левого поддерева
{
	if((*branch)->right)
		find_replace(&((*branch)->right), root);
	else
	{
		strcpy(root->word,(*branch)->word);
		tree_t *tmp = *branch;
		*branch = (*branch)->left;
		free(tmp);
	}
}

void remove_word(tree_t **root,char* search)  // удаление слова из дерева
{
	int cmp;
	tree_t *tmp;

	if (*root)
	{
		cmp = strcmp((*root)->word, search);
		if (cmp > 0)
			remove_word(&((*root)->right),search);
		else if (cmp < 0)
			remove_word(&((*root)->left),search);
		else
		{
			if (!(*root)->right)  // если нет правого потомка заменить на левый
			{
				tmp = (*root);
				*root = (*root)->left;
				free(tmp);
			}
			else if (!(*root)->left)   // если нет левого потомка, заменить на правый
			{
				tmp = (*root);
				*root = (*root)->right;
				free(tmp);
			}
			else
				find_replace(&((*root)->left), *root);  // найти самену в левом поддреве
		}
	}
}

void find_words(tree_t *root,char letter, int *count)  // поиск слова на нужную букву
{
	if (root)
	{
		find_words(root->left, letter, count);
		find_words(root->right,letter, count);
		if (root->word[0] == letter)
			*count += 1;
	}
}

tree_t* search_word(tree_t *root, char* search)  // поиск узлов
{
	if (root)
	{
		if (strcmp(root->word, search) > 0)
			search_word(root->right, search);
		else if (strcmp(root->word, search) < 0)
			search_word(root->left, search);
		else
			return root;
	}
	else
		return NULL;
}

void find_in_file(FILE *f,char letter, int *count)  // поиск слов в файле
{
	char *string = (char*)calloc(MAX_LEN+2,sizeof(char));
	while(fgets(string, MAX_LEN+2,f))
	{
		if (*string == letter)
			*count += 1;
	}
	free(string);
}

void time_it()  // замер времени выполнения
{
	FILE *f_in;
	tree_t *tree;
	int rn = OK;
	char letter;
	int count;
	int64_t t1, t2, result = 0;

	f_in = fopen("in_5.txt", "r");
	if (f_in)
	{
		tree = read_tree(f_in, &rn);
		if (!rn)
		{
			rewind(f_in);
			printf("    Введите букву: ");
			letter = getchar();
			letter = getchar();
			if (letter != '\n')
			{
				if ((letter <'A' || letter >'Z') && (letter <'a' || letter >'z'))
				{
				   printf("    Введено недопустимое значение\n\n");
				   rn = NOT_WORD;
				}
				else
				{
					printf("    Время поиска слов, начинающихся на букву '%c' в мс:\n",letter);
					count = 0;
					for(int i = 0; i < 10; i++)
					{
						t1 = tick();
						find_words(tree, letter, &count);
						t2 = tick();
						result += t2 - t1; 
					}
					printf("    В дереве:");
					printf("%1c" "%" PRId64 " " "%1c", ' ',((result)/CLOCKS_PER_SEC)/10, ' ');
					count = 0;
					for(int i = 0; i < 10; i++)
					{
						t1 = tick();
						find_in_file(f_in, letter, &count);
						t2 = tick();
						result += t2 - t1; 
					}
					printf("    В файле:");
					printf("%1c" "%" PRId64 " " "%1c", ' ',((result)/CLOCKS_PER_SEC)/10, ' ');
					printf("\n\n\n\n");

				}
			}
		}
		fclose(f_in);
	}
	else
		printf("    Нет такого файла как in_1.txt \n\n");
}

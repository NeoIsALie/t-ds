#ifndef __DEFINE_FILE_H__
#define __DEFINE_FILE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 50
#define OK 0
#define WRONG_SYMB -1
#define CANNOT_OPEN -2
#define NOT_WORD -3
#define ERR_MEMORY -4
#define NO_INPUT -5

typedef struct tree tree_t;

struct tree
{
    char word[MAX_LEN + 1];
    tree_t *left;
    tree_t *right;
};


#endif
